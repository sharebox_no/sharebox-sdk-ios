
Sharebox iOS SDK
============

Sharebox SDK allows app developers to integrate Sharebox® service into their native iOS apps.

Sharebox simplifies your handover by offering a service completely managed by your phone.
App-controlled cabinets, placed at central locations, ensure you a safe and efficient handover
whenever you need.

Sharebox SDK provides functionality to book lockers for free for a limited period of time.
Users can book any lockers they want, however each free reservation will be converted into a paid
one once free period ends.

## Structure ##

This project is divided into 3 parts:

* __API module__ - plain Sharebox API implementation.
* __SDK module__ - advanced implementation on top of API module, includes basic storage,
login/registration flow, etc.
* __Sample app__ - simple app that demonstrates SDK usage and general service flow.


## Requirements

iOS 9+.

Xcode 9+.

## Running sample app ##

To run the example project, clone the repo, and run `pod install` from the Example directory first.

Open `xcworkspace` file instead of `xcodeproj`.

To build and run sample app you will need to set up extra params in your 'SBConfig.plist' file
in 'Example/ShareboxSDK' directory:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>BaseUrl</key>
	<string>[API base url, w/o leading slash]</string>
	<key>Login</key>
	<string>[API_LOGIN]</string>
	<key>Password</key>
	<string>[API_PASSWORD]</string>
</dict>
</plist>
```

> _Do not forget to replace `[API_LOGIN]` and `[API_PASSWORD]` with actual credentials provided by
Sharebox._

You may also set up optional API endpoint url:

```xml
<key>BaseUrl</key>
<string>[API base url, w/o leading slash]</string>
```


## Usage ##

* [Dependency](#markdown-header-dependency)
* [Configuration](#markdown-header-configuration)
* [Async execution](#markdown-header-async-execution)
* [API errors](#markdown-header-api-errors)
* [Login flow](#markdown-header-login-flow)
* [Consent](#markdown-header-consent) 
* [Phone verification](#markdown-header-phone-verification)
* [Login](#markdown-header-login)
* [Registration](#markdown-header-registration)
* [Profile](#markdown-header-profile)
* [Logout](#markdown-header-logout)
* [Locations](#markdown-header-locations)
* [Location details](#markdown-header-location-details)
* [Booking](#markdown-header-booking)
* [Reservations list](#markdown-header-reservations-list)
* [Reservation details](#markdown-header-reservation-details)
* [Reservation actions](#markdown-header-reservation-actions)
* [User location](#markdown-header-user-location)
* [Useful constants](#markdown-header-useful-constants)


### Dependency ###

Add a new dependency into your app's Podfile file:

```ruby
pod 'ShareboxSDK'
```

If you only want to use API module then add the following dependency instead:

```ruby
pod 'ShareboxSDK/API'
```

### Configuration ###

Now we need to initialize SDK in `AppDelegate - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions` method:

```objective-c
SBApiClientConfiguration *configuration = [[SBApiClientConfiguration alloc] initWithBaseUrl:nil login:@"[API_LOGIN]" password:@"[API_PASSWORD]"];
SBSDK.shared.configuration = configuration;
```

> _Do not forget to replace `[API_LOGIN]` and `[API_PASSWORD]` with actual credentials provided by
Sharebox._

You can also initialize SDK by provide `[API_LOGIN]` and `[API_PASSWORD]` via `plist` file.

```objective-c
SBApiClientConfiguration *configuration = [SBApiClientConfiguration configurationWithPlist:@"SBConfig.plist"];
```

or using default plist file name. The app must import file "SBConfig.plist".

```objective-c
SBApiClientConfiguration *configuration = [SBApiClientConfiguration defaultConfiguration];
```

_Note that configuration plist must have following format_
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>BaseUrl</key>
	<string>[API base url, w/o leading slash]</string>
	<key>Login</key>
	<string>[API_LOGIN]</string>
	<key>Password</key>
	<string>[API_PASSWORD]</string>
</dict>
</plist>
```

Once initialized `SBSDK` instance can be retrieved with `SBSDK.shared` method.

If you're only planing to use API module then initialize `ShareboxApi` directly:

```objective-c
SBApiClientConfiguration *configuration = [[SBApiClientConfiguration alloc] initWithBaseUrl:nil login:@"[API_LOGIN]" password:@"[API_PASSWORD]"];
SBApiClient.defaultClient.configuration = configuration;
```

Once initialized `SBApiClient` instance can be retrieved with `SBApiClient.defaultClient` method.


### Async execution ###

All SDK methods can be called from the main thread, but some of the methods should perform heavy
tasks (like calling API methods) and will actually be executed in background thread.

Such methods will return `id<SBCancelable>` object instead of the result itself so that we can provide necessary
callbacks and request background execution.

A task will be executed immediately when we'll call the method.

Typical task execution will look like this:

```objective-c
SBWeakify(self);
[SBSDK.shared getProfile:^(id userProfile, NSError *error) {
    SBStrongify(self);
    if (error) {
        //handle the error
    } else {
        //handle the result
    }
}];
```

Since tasks are executed in background thread we can potentially leak our callbacks references,
which in turn can contain references to current activity leading to memory leaks.
To avoid such leaks, method return `id<SBCancelable>` instance which should be used
to cancel execution when we are leaving current activity and don't want our callbacks to be called
anymore.

```objective-c
SBWeakify(self);
id<SBCancelable> *rq = [SBSDK.shared getProfile:^(id userProfile, NSError *error) {
    SBStrongify(self);
    if (error) {
        //handle the error
    } else {
        //handle the result
    }
}];

[rq cancel];
```


### API errors ###

Sharebox API may return predefined error codes if something goes wrong (ref. API specification),
in this case `NSError` will be thrown.

* And error code can be accessed with `code` propety.
* Error domain can be accessed with `domain` propety.
* Other informations can be accessed with `userInfo` property.

### Login flow ###

Before the user can start using the service we need to authorize them in Sharebox.
But before logging in we also need to collect user's consent to Sharebox terms,
so the overall flow looks like this:

* [Consent](#markdown-header-consent) - Asking the user to provide their consent to Sharebox terms. 
* [Phone verification](#markdown-header-phone-verification) - Requesting SMS code to verify user's
phone number.
* [Login](#markdown-header-login) - Verifying SMS code and trying to log the user in.
* [Registration](#markdown-header-registration) - Registering the user, if not registered.


### Consent ###

To load Sharebox consent use the following SDK method:

```objective-c
- (nonnull id<SBCancelable>)getConsentWithCompletion:(nonnull SBGetConsentCompletion)completion;
```

> _Note, that this method returns `id<SBCancelable>` instance, check
[Async execution](#markdown-header-async-execution) section for more information on how to deal
with callbacks._

The app must clearly present consent's text to the user and the user must explicitly accept it to
be able to use Sharebox service. The app must not grant consent on user behalf and must not
pre-check related checkbox or similar control.

Once the user grants their consent we should save version of the consent text
(`[Consent versionCode]`) to be used further.

### Phone verification ###

Once consent is collected we can proceed with phone verification:

```objective-c
- (nonnull id<SBCancelable>)authenticatePhone:(nonnull NSString *)phone
                               consentVersion:(NSInteger)consentVersion
                                     deviceId:(nonnull NSString *)deviceId
                                   completion:(SBCompletion _Nonnull)completion;
```

> _Note, that provided phone number should be in international format starting with country code
prefixed with + sign and can only contain digits and the + sign itself. For example: `+4712345678`_

The user should now receive SMS with 5-digits code on this phone number to actually verify the phone.

We can check that SMS code was requested but was not yet verified with:

```objective-c
- (BOOL)isVerificationRequested;
```

We can also access the phone number used for verification with:

```objective-c
- (nullable NSString *)phoneToVerify;
```


### Login ###

Once the user provided phone verification code received by SMS we can try to log them in:

```objective-c
- (nonnull id<SBCancelable>)loginWithSMSCode:(nonnull NSString *)code
                                    deviceId:(nonnull NSString *)deviceId
                                  completion:(SBCompletion _Nonnull)completion;
```

If the user was already registered in Sharebox then this method will be successfully executed and
the user will be authorized.

We can check that the user is logged in with:

```objective-c
- (BOOL)isLoggedIn;
```

But if the user was not registered yet login method will return error with
code `SB_API_ERROR_NOT_REGISTER` and we should ask the user to register
(see [Registration](#markdown-header-registration) section).

We can check that the user successfully verified their phone number but didn't register yet with:

```objective-c
- (BOOL)isRegistrationNeeded;
```

If SMS code provided by the user is invalid API will return error with code
`SB_API_ERROR_INVALID_SMS_CODE`.


### Registration ###

If we detected that the user is not registered we should ask them to provide first name, last name
and email address and then register this information with:

```objective-c
- (nonnull id<SBCancelable>)registerWithFirstName:(nonnull NSString *)firstName
                                         lastname:(nonnull NSString *)lastName
                                            email:(nonnull NSString *)email
                                       completion:(nonnull SBCompletion)completion;
```

Once registered the user will be automatically authorized and we can start using Sharebox service.


### Profile ###

Once the user is logged in (or registered) we should already have profile information loaded
and cached locally.

To get cached profile details we can use:

```objective-c
- (nullable SBUserProfile *)user;
```

To update user profile details from the server we can use the following method:

```objective-c
- (nonnull id<SBCancelable>)getProfile:(nonnull SBGetProfileCompletion)completion;
```

> _See [Async execution](#markdown-header-async-execution) section for more information
about `id<SBCancelable>` instance._


__Important: Consent__

Every time user profile is updated from the server we should check that we have user consent for
the latest version of Sharebox terms with `SBUserProfile.consentGranted` property. If this method
returns `NO` we should load consent text as described in [Consent](#markdown-header-consent)
section above, ask the user to accept it and then save it back into user profile with:

```objective-c
- (nonnull id<SBCancelable>)grantConsentVersion:(NSInteger)version completion:(nonnull SBCompletion)completion;
```

If the user does not accept new terms we should log them out from Sharebox. 

__Important: Blocked users__

The user can be blocked from using the service (for example the user forgot to pay for
a reservation), in this case they will not be allowed to create new reservations but will still be
able to load and control existing reservations.

If the user is blocked method `SBUserProfile.status` will return `SBUserStatusBlocked` status.

```objective-c
@property(nonatomic) SBUserStatus status;
```

### Logout ###

To log the user out and clear all user data saved locally we should use the following method:

```objective-c
- (void)logout;
```

This method can also be used at any point during login and registration flow to reset the progress.


### Locations ###

To start reservation process the user should pick one of the available locations within single
stores chain (as defined by API credentials).
Available locations list can be loaded with the following method:

```objective-c
- (nonnull id<SBCancelable>)getLocations:(nonnull SBGetLocationsCompletion)completion;
```

By default locations list is sorted by name, but it is preferable to sort locations by distance.
See [User location](#markdown-header-user-location) section for information on how to get current
user location.

Once we know user location we can compute locations distances with:

```objective-c
+ (CLLocationDistance)distanceFromUserLocation:(CLLocationCoordinate2D)location toStore:(nonnull SBStoreLocation *)store;
```

And compute sorted locations list with:

```objective-c
+ (nonnull NSArray<SBStoreLocation *> *)sortStores:(nonnull NSArray<SBStoreLocation *> *)stores byDistanceToUserLocation:(CLLocationCoordinate2D)location;
```


### Location details ###

When the user picks particular location we should load it's details to check cabinets and boxes
availability for this location:

```objective-c
- (nonnull id<SBCancelable>)getLocationDetailWithId:(NSInteger)locationId completion:(nonnull SBGetLocationDetailCompletion)completion;
```

Location details loaded with this method will contain correct cabinets availability info which can
be checked with `[StoreLocation firstAvailableCarbinet]` method: if this method returns `null` then
we don't have available boxes in this location and the user should not be able to reserve boxes here.


### Booking ###

Once the user picked desired location and we checked availability for this location we can proceed
with box reservation.

The user needs to provide reservation name (for example description of the box content) and specify
phone number of the person they want to share the box with. Once we have all the required
information we can create a new reservation with:

```objective-c
- (nonnull id<SBCancelable>)reserveLockerWithCabinetId:(NSInteger)cabinetId
                                        receiverPhones:(nonnull NSArray<NSString *> *)phones
                                       reservationName:(nonnull NSString *)name
                                            completion:(nonnull SBReserveLockerCompletion)completion;
```

Where `cabinetId` is ID of the cabinet found with `[StoreLocation firstAvailableCarbinet]` method.

> _Note, that provided phone number should be in international format starting with country code
prefixed with + sign and can only contain digits and the + sign itself. For example: `+4712345678`_

Once a new reservation is created we should provide basic instructions to the user and redirect them
to the reservation details. Next user's step will be to open the box and put their stuff into it.


### Reservations list ###

Active user reservations can be loaded with the following method:

```objective-c
- (nonnull id<SBCancelable>)getReservations:(nonnull SBGetReservationsCompletion)completion;
```

List of last several non-active reservations can be loaded with:

```objective-c
- (nonnull id<SBCancelable>)getReservations:(nonnull SBGetReservationsCompletion)completion;
```

> _Note, that each reservation may potentially have more than 1 recipient, but only first
recipient will be provided when loading reservations list, to load all recipients you will need
to separately load reservation details,
see [Reservation details](#markdown-header-reservation-details) section._


### Reservation details ###

We can use the following method to load single reservation details and to load a list of
corresponding reservation events (for example "reserved by" and "box opened by"):

```objective-c
- (nonnull id<SBCancelable>)getReservationDetailWithId:(NSInteger)reservationId completion:(nonnull SBReservationDetailCompletion)completion;
```


### Reservation actions ###

List of available reservation's actions depends on whether current user is the owner or
the recipient of the reservation.

Owner's actions:

* Open the box
* Open the box and finish the reservation

Recipient's actions:

* Open the box (reservation will be finished automatically)

You can use the following `SBReservation` methods to check if it is possible to perform any of these actions:

```objective-c
- (BOOL)canOpen;
- (BOOL)canFinish;
```

Both actions can be performed using the same SDK method:

```objective-c
- (nonnull id<SBCancelable>)openLockerWithId:(NSInteger)lockerId
                                    latitude:(nonnull NSNumber *)latitude
                                   longitude:(nonnull NSNumber *)longitude
                                   forceOpen:(BOOL)forceOpen
                           finishReservation:(BOOL)isFinishReservation
                                  completion:(nonnull SBCompletion)completion;
```

__Important: user location__

When opening a locker we should send current user location so that the server can check that
the user is close enough to the locker to open it, otherwise API will return
error with code `SB_API_ERROR_TOO_FAR_FROM_LOCKER`.

See [User location](#markdown-header-user-location) section for information on how to get current
user location.

If the user is not close enough or we don't have access to current user location then we still
can open the locker by set param `forceOpen:YES`, but the user should be
notified about the problem and should explicitly agree to proceed with the action anyway. 

If the owner only want to stop subscription without open the box, using method

```objective-c
- (nonnull id<SBCancelable>)stopSubscription:(NSInteger)subscriptionId 
                                  completion:(nonnull SBCompletion)completion;
```
### User location ###

We need to know user location to sort locations list by their distances
(see [Locations](#markdown-header-locations) section) and when performing actions on reservations
(see [Reservation actions](#markdown-header-reservation-actions) section).
 
To access user location, we need provide the reason to `Info.plist` of host app.

```xml
<key>NSLocationWhenInUseUsageDescription</key>
<string>Provide your reason to access user location</string>
```

And use `CLLocationManager` to request access and start update user location.

```objective-c
CLLocationManager *locationManager = [CLLocationManager new];
[locationManager requestWhenInUseAuthorization];
[locationManager startUpdatingLocation];
locationManager.delegate = self;
```

Then access user location in the delegate method:

```objective-c
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations;
```

See more [Location and Maps Programming Guide](https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/LocationAwarenessPG/CoreLocation/CoreLocation.html).


### Useful constants ###

`SBSDK` class provides few useful constants:

* `FREE_HOURS` - Number of free hours for each reservation
* `TERMS_URL` - Link to terms and conditions page.
* `SUPPORT_PHONE_NO` - Support phone number for Norway.
* `SUPPORT_PHONE_DK` - Support phone number for Denmark.


## License ##

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

