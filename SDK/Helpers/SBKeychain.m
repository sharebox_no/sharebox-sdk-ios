//
//  SBKeychain.m
//  ShareboxSDK


#import "SBKeychain.h"
#import <Security/Security.h>

NSString * const SB_KEYCHAIN_SERVICE = @"com.sharebox.keychain_service";

@implementation SBKeychain

+ (instancetype)shared {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [SBKeychain new];
    });
    
    return sharedInstance;
}

- (NSDictionary *)keychainQueryForKey:(NSString *)key {
    return @{(__bridge NSString *)kSecAttrService : SB_KEYCHAIN_SERVICE,
             (__bridge NSString *)kSecClass : (__bridge id)kSecClassGenericPassword,
             (__bridge NSString *)kSecAttrAccount : key};
}
- (NSData *)dataForKey:(NSString *)key
{
    //generate query
    NSMutableDictionary *query = [NSMutableDictionary dictionaryWithDictionary:[self keychainQueryForKey:key]];
    query[(__bridge NSString *)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    query[(__bridge NSString *)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    
    //recover data
    CFDataRef data = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&data);
    if (status != errSecSuccess && status != errSecItemNotFound) {
        NSLog(@"Keychain failed to retrieve data for key '%@', error: %ld", key, (long)status);
    }
    return CFBridgingRelease(data);
}

- (BOOL)setData:(NSData *)data forKey:(NSString *)key {
    //generate query
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    query[(__bridge NSString *)kSecAttrService] = SB_KEYCHAIN_SERVICE;
    query[(__bridge NSString *)kSecClass] = (__bridge id)kSecClassGenericPassword;
    query[(__bridge NSString *)kSecAttrAccount] = [key description];
    
    //fail if object is invalid
    //print error
    if (data) {
        //update values
        NSMutableDictionary *update = [@{(__bridge NSString *)kSecValueData: data} mutableCopy];
        
        //write data
        OSStatus status = errSecSuccess;
        if ([self dataForKey:key]) {
            //there's already existing data for this key, update it
            status = SecItemUpdate((__bridge CFDictionaryRef)query, (__bridge CFDictionaryRef)update);
        } else {
            //no existing data, add a new item
            [query addEntriesFromDictionary:update];
            status = SecItemAdd ((__bridge CFDictionaryRef)query, NULL);
        }

        if (status != errSecSuccess) {
            NSLog(@"Keychain failed to store data for key '%@', error: %ld", key, (long)status);
            return NO;
        }
    } else if ([self dataForKey:key]) {
        //delete existing data
        
        OSStatus status = SecItemDelete((__bridge CFDictionaryRef)query);
        if (status != errSecSuccess) {
            NSLog(@"Keychain failed to delete data for key '%@', error: %ld", key, (long)status);
            return NO;
        }
    }
    return YES;
}

- (BOOL)setString:(NSString *)string forKey:(NSString *)key {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [self setData:data forKey:key];
}

- (NSString *)stringForKey:(NSString *)key {
    NSData *data = [self dataForKey:key];
    if (data) {
        return [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    } else {
        return nil;
    }
}
@end
