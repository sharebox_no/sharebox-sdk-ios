//
//  SBKeychain.h
//  ShareboxSDK


#import <Foundation/Foundation.h>

@interface SBKeychain : NSObject

+ (nonnull instancetype)shared;
- (nonnull instancetype)init __attribute((unavailable("Not available. Using shared instance")));
- (BOOL)setData:(nullable NSData *)data forKey:(nonnull NSString *)key;
- (nullable NSData *)dataForKey:(nonnull NSString *)key;
- (BOOL)setString:(nullable NSString *)string forKey:(nonnull NSString *)key;
- (nullable NSString *)stringForKey:(nonnull NSString *)key;
@end
