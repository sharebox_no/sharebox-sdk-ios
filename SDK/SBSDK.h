//
//  SBSDK.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBApiClientConfiguration.h"
#import "SBCommon.h"
#import "SBApiClient+Endpoint.h"

@class SBUserProfile;
@class SBApiClient;
@class SBStoreLocation;
@class SBReservation;
@class SBReservationEvent;

@interface SBSDK : NSObject
///shared instance
+ (nonnull instancetype)shared;
- (nonnull instancetype)init __attribute((unavailable("Not available. Using shared instance instead")));
@property (nonatomic, nonnull) SBApiClientConfiguration *configuration;
@end

/**
 Storage methods
 */
@interface SBSDK (Storage)
///return true if user logged in.
- (BOOL)isLoggedIn;

///return true if phone number verifications is in progress.
- (BOOL)isVerificationRequested;

/**
 return the phone which is currently in verfification phase (sms code was requested).
 
 If user logged in, the value will be clear. Use phone number in current user info instead.
 */
- (nullable NSString *)phoneToVerify;

///return true if user successfully verified their phone number but didn't register yet.
- (BOOL)isRegistrationNeeded;

///return current user profile. Null if user not logged in.
- (nullable SBUserProfile *)user;

///logout user and clean all cache data.
- (void)logout;
@end

/**
 API methods
 */
@interface SBSDK (API)

///simple success completion.
typedef void (^SBCompletion)(NSError *_Nullable);

///get consent completion.
typedef void (^SBGetConsentCompletion)(SBConsent *_Nullable, NSError *_Nullable);

///get user profile completion.
typedef void (^SBGetProfileCompletion)(SBUserProfile *_Nullable, NSError *_Nullable);

///reserve locker completion.
typedef void (^SBReserveLockerCompletion)(SBReserveLockerInfo *_Nullable, NSError *_Nullable);

///get locations completion.
typedef void (^SBGetLocationsCompletion)(NSArray<SBStoreLocation *> *_Nullable, NSError *_Nullable);

///get location detail completion.
typedef void (^SBGetLocationDetailCompletion)(SBStoreLocation *_Nullable, NSError *_Nullable);

///get reservations completion
typedef void (^SBGetReservationsCompletion)(NSArray<SBReservation *> *_Nullable, NSError *_Nullable);

///get reservation detail completion
typedef void (^SBReservationDetailCompletion)(SBReservationInfo *_Nullable, NSError *_Nullable);


/**
 Get the latest Consent

 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getConsentWithCompletion:(nonnull SBGetConsentCompletion)completion;


/**
 Requests phone verification code to be sent by SMS.

 @param phone phone to send sms.
 @param consentVersion consent version.
 @param deviceId deviceId.
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)authenticatePhone:(nonnull NSString *)phone
                               consentVersion:(NSInteger)consentVersion
                                     deviceId:(nonnull NSString *)deviceId
                                   completion:(SBCompletion _Nonnull)completion;


/**
 Login

 @param code SMS code that receive from verify phone number.
 @param deviceId device Id. Must be same with verify phone number step.
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)loginWithSMSCode:(nonnull NSString *)code
                                    deviceId:(nonnull NSString *)deviceId
                                  completion:(SBCompletion _Nonnull)completion;

/**
 Register user and log them in.

 @param firstName first name
 @param lastName last name
 @param email email
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)registerWithFirstName:(nonnull NSString *)firstName
                                         lastname:(nonnull NSString *)lastName
                                            email:(nonnull NSString *)email
                                       completion:(nonnull SBCompletion)completion;


/**
 Update the latest user profile from server.

 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getProfile:(nonnull SBGetProfileCompletion)completion;


/**
 Grant the latest consent to user.

 @param version version of consent
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)grantConsentVersion:(NSInteger)version completion:(nonnull SBCompletion)completion;


/**
 Get locations list sorted by name.

 @param completion a block contains list of locations. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getLocations:(nonnull SBGetLocationsCompletion)completion;


/**
 Get location detail, including cabinets and their availability

 @param locationId Location Id
 @param completion a block contains location detail info. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getLocationDetailWithId:(NSInteger)locationId completion:(nonnull SBGetLocationDetailCompletion)completion;


/**
 Create new locker reservation

 @param cabinetId cabinet Id
 @param phones phones that share the locker
 @param name name of the locker
 @param completion a block contains locker reservation info. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)reserveLockerWithCabinetId:(NSInteger)cabinetId
                                        receiverPhones:(nonnull NSArray<NSString *> *)phones
                                       reservationName:(nonnull NSString *)name
                                            completion:(nonnull SBReserveLockerCompletion)completion;

/**
 Returns list of user's active reservations.

 @param completion a block contains user active reservations. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getReservations:(nonnull SBGetReservationsCompletion)completion;


/**
 Returns list of user's recently finished reservations.
 
 @param completion a block contains reservations. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getReservationsHistory:(nonnull SBGetReservationsCompletion)completion;


/**
 Return reservation detail.

 @param reservationId reservation id.
 @param completion a block contains reservation detail info. Block is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)getReservationDetailWithId:(NSInteger)reservationId completion:(nonnull SBReservationDetailCompletion)completion;


/**
 Open an locker.

 @param lockerId locker Id.
 @param latitude latitude of user.
 @param longitude longitude of user.
 @param forceOpen force open. If yes, open the locker regardless user location.
 @param isFinishReservation If yes, open and then finish the locker.
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)openLockerWithId:(NSInteger)lockerId
                                    latitude:(nonnull NSNumber *)latitude
                                   longitude:(nonnull NSNumber *)longitude
                                   forceOpen:(BOOL)forceOpen
                           finishReservation:(BOOL)isFinishReservation
                                  completion:(nonnull SBCompletion)completion;

/**
 Stop a subscription.

 @param subscriptionId subscription Id.
 @param completion a block which is invoked when the request finishes, or error. Invoked asynchronously on the main thread in the future.
 @return an cancelable instance. Use [intance cancel] to cancel request immediately. The completion block should not fire this case.
 */
- (nonnull id<SBCancelable>)stopSubscription:(NSInteger)subscriptionId completion:(nonnull SBCompletion)completion;
@end

/**
 Constants
 */
@interface SBSDK (Constants)

///return number of free hours for each reservation.
+ (NSInteger)FREE_HOURS;

///link to terms and conditions page.
+ (nonnull NSString *)TERMS_URL;

///Support phone number for Norway.
+ (nonnull NSString *)SUPPORT_PHONE_NO;

///Support phone number for Denmark.
+ (nonnull NSString *)SUPPORT_PHONE_DK;
@end
