//
//  SBSDK.m
//  ShareboxSDK


#import "SBSDK.h"
#import "SBCommon.h"
#import "SBApiClient.h"
#import "NSString+SBNetwork.h"
#import "NSURLRequest+SBRequestable.h"
#import "NSError+SB.h"
#import "SBKeychain.h"
#import "SBUserProfile.h"
#import "SBConsent.h"
#import "SBReservation.h"

/**
 Storage keys
 */
static NSString *const SBStorageUserProfileKey = @"SBStorageUserProfileKey";
static NSString *const SBStoragePhoneKey = @"SBStoragePhoneKey";
static NSString *const SBStorageDigitsKey = @"SBStorageDigitsKey";
static NSString *const SBStorageSessionIdKey = @"SBStorageSessionIdKey";

@interface SBSDK()
@property (nonatomic) SBApiClient *apiClient;

@property(nonatomic) NSString *sessionId;
@property(nonatomic) NSString *digitsId;
@property(nonatomic) NSString *phone;
@property(nonatomic) SBUserProfile *userProfile;
@end

@implementation SBSDK
@synthesize configuration = _configuration;

+ (instancetype)shared {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if(self) {
        [self loadStorage];
        _apiClient = SBApiClient.defaultClient;
    }
    return self;
}

- (void)setPhone:(NSString *)phone {
    _phone = phone;
    [[SBKeychain shared] setString:phone forKey:SBStoragePhoneKey];
}
- (void)setSessionId:(NSString *)sessionId {
    _sessionId = sessionId;
    [[SBKeychain shared] setString:sessionId forKey:SBStorageSessionIdKey];
}

- (void)setDigitsId:(NSString *)digitsId {
    _digitsId = digitsId;
    [[SBKeychain shared] setString:digitsId forKey:SBStorageDigitsKey];
}

- (void)setUserProfile:(SBUserProfile *)userProfile {
    _userProfile = userProfile;
    NSDictionary *dictionary = [userProfile json];
    if (dictionary) {
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
        [[SBKeychain shared] setData:data forKey:SBStorageUserProfileKey];
    } else {
        [[SBKeychain shared] setData:nil forKey:SBStorageUserProfileKey];
    }
}

- (SBApiClientConfiguration *)configuration {
    if (_configuration) {
        return _configuration;
    } else {
        [NSException raise:@"Wrong Sharebox Configuration" format:@"No configuration found"];
    }
    return nil;
}

- (void)setConfiguration:(nonnull SBApiClientConfiguration *)configuration {
    _configuration = configuration;
    _apiClient.configuration = configuration;
}

- (void)loadStorage {
    SBKeychain *keychain = [SBKeychain shared];
    _phone = [keychain stringForKey:SBStoragePhoneKey];
    _digitsId = [keychain stringForKey:SBStorageDigitsKey];
    _sessionId = [keychain stringForKey:SBStorageSessionIdKey];
    NSData *userProfile = [keychain dataForKey:SBStorageUserProfileKey];
    if (userProfile) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:userProfile options:NSJSONReadingAllowFragments error:nil];
        if (json) {
            _userProfile = [SBUserProfile initWithJson:json];
        }
    }
}
@end

/*
 Storage methods
 */
@implementation SBSDK (Storage)

- (BOOL)isLoggedIn {
    return self.sessionId.length > 0;
}

- (BOOL)isVerificationRequested {
    return (self.phoneToVerify.length > 0) && (self.digitsId.length == 0);
}

- (NSString *)phoneToVerify {
    return self.phone;
}

- (BOOL)isRegistrationNeeded {
    return !self.isLoggedIn && (self.digitsId.length > 0);
}

- (SBUserProfile *)user {
    return _userProfile;
}

- (void)logout {
    [self.apiClient reset];
    [self setPhone:nil];
    [self setSessionId:nil];
    [self setDigitsId:nil];
    [self setUserProfile:nil];
}
@end

/*
 API methods
 */

@implementation SBSDK (API)
//MARK: Fadcades
- (nonnull id<SBCancelable>)getConsentWithCompletion:(nonnull SBGetConsentCompletion)completion {
//    SBWeakify(self);
    return [self.apiClient getConsentWithCompletion:^(SBRequest *rq, SBConsent *consent, NSError *error) {
//        SBStrongify(self);
        completion(consent, error);
    }];
}

- (id<SBCancelable>)authenticatePhone:(NSString *)phone
                          consentVersion:(NSInteger)consentVersion
                                deviceId:(NSString *)deviceId
                              completion:(SBCompletion)completion {
    NSDictionary *params = @{@"mobile" : phone, @"device_id": deviceId, @"consent_version": @(consentVersion)};
    
    SBWeakify(self);
    return [self.apiClient authenticatePhone:params completion:^(SBRequest *rq, NSError *error) {
        SBStrongify(self);
        BOOL success = error == nil;
        if (success) {
            [self setPhone:phone];
        }
        completion(error);
    }];

}

- (id<SBCancelable>)authenticateSMSCode:(NSString *)code deviceId:(NSString *)deviceId completion:(SBCompletion)completion {
    NSDictionary *params = @{@"sms_code": code, @"mobile" : self.phone, @"device_id": deviceId};
    SBWeakify(self);
    return [self.apiClient authenticateSMS:params completion:^(SBRequest *rq, id json, NSError *error) {
        SBStrongify(self);
        NSString *digitsId = json[@"digits_id"];
        BOOL success = error == nil;
        if (success) {
            [self setDigitsId:digitsId];
        }
        completion(error);
    }];
}

- (id<SBCancelable>)login:(SBCompletion)completion {
    //if digitsId emplty -> throw exception
    NSDictionary *params = @{@"digits_id" : self.digitsId};

    SBWeakify(self);
    return [self.apiClient login:params completion:^(SBRequest *rq, SBLoginInfo *loginInfo, NSError *error) {
        SBStrongify(self);
        BOOL success = error == nil;
        if (success) {
            [self setSessionId:loginInfo.sessionId];
            [self setUserProfile:loginInfo.userProfile];
            //clear phone token
            [self setDigitsId:nil];
            //clear the verify phone
            [self setPhone:nil];
        }
        completion(error);
    }];
}

- (id<SBCancelable>)loginWithSMSCode:(NSString *)code deviceId:(NSString *)deviceId completion:(SBCompletion)completion {
    NSDictionary *params = @{@"sms_code": code, @"mobile" : self.phone, @"device_id": deviceId};
    SBWeakify(self);
    id<SBCancelable> verifySMS = [self.apiClient authenticateSMS:params completion:^(SBRequest *rq, id json, NSError *error) {
        SBStrongify(self);
        NSString *digitsId = json[@"digits_id"];
        BOOL success = error == nil;
        if (success) {
            [self setDigitsId:digitsId];
            [self login:completion];
        } else {
            completion(error);
        }
    }];
    return verifySMS;
}

- (id<SBCancelable>)registerWithFirstName:(nonnull NSString *)firstName
                                    lastname:(nonnull NSString *)lastName
                                       email:(nonnull NSString *)email
                                  completion:(nonnull SBCompletion)completion {
    //if digitsId emplty -> throw exception
    NSDictionary *params = @{@"digits_id": self.digitsId, @"first_name": firstName, @"last_name": lastName, @"email": email};
    
    SBWeakify(self);
    return [self.apiClient registerUser:params completion:^(SBRequest *rq, id json, NSError *error) {
        SBStrongify(self);
        BOOL success = error == nil;
        if (success) {
            NSString *sessionId = json[@"session_id"];
            [self setSessionId:sessionId];
            //clear phone token
            [self setDigitsId:nil];
            //clear the verify phone
            [self setPhone:nil];
        }
        completion(error);
    }];
}

- (id<SBCancelable>)getProfile:(SBGetProfileCompletion)completion {
    SBWeakify(self);
    return [self.apiClient getProfileWithSessionId:self.sessionId completion:^(SBRequest *rq, id userProfile, NSError *error) {
        SBStrongify(self);
        if (userProfile && !error) {
            [self setUserProfile:userProfile];
        }
        completion(userProfile, error);
    }];
}

- (id<SBCancelable>)grantConsentVersion:(NSInteger)version completion:(SBCompletion)completion {
    NSDictionary *params = @{@"version_code": @(version)};
    SBWeakify(self);
    return [self.apiClient grantConsent:params sessionId:self.sessionId completion:^(SBRequest *rq, NSError *error) {
        SBStrongify(self);
        BOOL success = error == nil;
        if (success) {
            SBUserProfile *profile = self.userProfile;
            profile.consentGranted = YES;
            [self setUserProfile:profile];
        }
        completion(error);
    }];
}

- (id<SBCancelable>)getLocations:(SBGetLocationsCompletion)completion {
    return [self.apiClient getLocationsWithSessionId:self.sessionId completion:^(SBRequest *rq, NSArray *locations, NSError *error) {
        completion(locations, error);
    }];
}

- (id<SBCancelable>)getLocationDetailWithId:(NSInteger)locationId completion:(SBGetLocationDetailCompletion)completion {
    NSDictionary *params = @{@"location_id": @(locationId)};
    return [self.apiClient getLocationDetail:params sessionId:self.sessionId completion:^(SBRequest *rq, id location, NSError *error) {
        completion(location, error);
    }];
}

- (id<SBCancelable>)reserveLockerWithCabinetId:(NSInteger)cabinetId
                    receiverPhones:(NSArray<NSString *> *)phones
                   reservationName:(NSString *)name
                        completion:(SBReserveLockerCompletion)completion {
    NSDictionary *params = @{@"cabinet_id":@(cabinetId), @"receiving_mob_numbers":phones, @"name": name,
                             @"content":@"other", //hardcode params
                             @"reservation_type":@(3) //hardcode params
                             };
    return [self.apiClient reserveLocker:params sessionId:self.sessionId completion:^(SBRequest *rq, id obj, NSError *error) {
        completion(obj, error);
    }];
}

- (id<SBCancelable>)getReservations:(SBGetReservationsCompletion)completion {
    return [self.apiClient getReservationsWithSessionId:self.sessionId completion:^(SBRequest *rq, NSArray *reservations, NSError *error) {
        NSPredicate *unknownTypeFilter = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable reservation, NSDictionary<NSString *,id> * _Nullable bindings) {
            return ((SBReservation *)reservation).type != SBReservationTypeUnknown;
        }];
        NSArray *result = [reservations filteredArrayUsingPredicate:unknownTypeFilter];
        completion(result, error);
    }];
}

- (id<SBCancelable>)getReservationsHistory:(SBGetReservationsCompletion)completion {
    return [self.apiClient getReservationsHistoryWithSessionId:self.sessionId completion:^(SBRequest *rq, NSArray *reservations, NSError *error) {
        NSPredicate *unknownTypeFilter = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable reservation, NSDictionary<NSString *,id> * _Nullable bindings) {
            return ((SBReservation *)reservation).type != SBReservationTypeUnknown;
        }];
        NSArray *result = [reservations filteredArrayUsingPredicate:unknownTypeFilter];
        completion(result, error);
    }];
}

- (id<SBCancelable>)getReservationDetailWithId:(NSInteger)reservationId completion:(SBReservationDetailCompletion)completion {
    NSDictionary *params = @{@"reservation_id": @(reservationId)};
    return [self.apiClient getReservationDetail:params sessionId:self.sessionId completion:^(SBRequest *rq, SBReservationInfo *info, NSError *error) {
        completion(info, error);
    }];
}

- (id<SBCancelable>)openLockerWithId:(NSInteger)lockerId
            latitude:(NSNumber *)latitude
            longitude:(NSNumber *)longitude
            forceOpen:(BOOL)forceOpen
    finishReservation:(BOOL)isFinishReservation
            completion:(SBCompletion)completion {
    NSNumber *forceFlag = forceOpen ? @1 : @0;
    NSNumber *finishFlag = isFinishReservation ? @1 : @0;
    NSDictionary *params = @{@"locker_id": @(lockerId), @"latitude": latitude, @"longitude": longitude,
                             @"flag": forceFlag, @"finish_flag": finishFlag};
    return [self.apiClient openLocker:params sessionId:self.sessionId completion:^(SBRequest *rq, NSError *error) {
        completion(error);
    }];
}

- (id<SBCancelable>)stopSubscription:(NSInteger)subscriptionId completion:(SBCompletion)completion {
    NSDictionary *params = @{@"subscription_id": @(subscriptionId)};
    return [self.apiClient stopSubscription:params sessionId:self.sessionId completion:^(SBRequest *rq, NSError *error) {
        completion(error);
    }];
}
@end

@implementation SBSDK (Constants)
+ (NSInteger)FREE_HOURS {
    return 12;
}

+ (nonnull NSString *)TERMS_URL {
    return @"https://sharebox.no/terms/";
}

+ (nonnull NSString *)SUPPORT_PHONE_NO {
    return @"+4740434444";
}

+ (nonnull NSString *)SUPPORT_PHONE_DK {
    return @"+4577340722";
}
@end
