//
//  SBSDK+Utilities.m
//  ShareboxSDK


#import "SBSDK+Utilities.h"
#import "SBStoreLocation.h"

/*
 Utilities methods
 */
@implementation SBSDK (Utilities)
+ (BOOL)isValidPhoneFormat:(NSString *)phone {
    return [phone hasPrefix:@"+"] && phone.length > 1;
}

+ (CLLocationDistance)distanceFromUserLocation:(CLLocationCoordinate2D)location toStore:(SBStoreLocation *)store {
    CLLocation *l1 = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    CLLocation *l2 = [[CLLocation alloc] initWithLatitude:store.latitude.doubleValue longitude:store.longitude.doubleValue];
    return [l1 distanceFromLocation:l2];
}

+ (NSArray<SBStoreLocation *> *)sortStores:(NSArray<SBStoreLocation *> *)stores byDistanceToUserLocation:(CLLocationCoordinate2D)location {
    return [stores sortedArrayUsingComparator:^NSComparisonResult(SBStoreLocation *store1, SBStoreLocation *store2) {
        
        CLLocationDistance d1 = [self distanceFromUserLocation:location toStore:store1];
        CLLocationDistance d2 = [self distanceFromUserLocation:location toStore:store2];
        
        if (d1 < d2) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
    }];
}
@end
