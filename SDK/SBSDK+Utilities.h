//
//  SBSDK+Utilities.h
//  ShareboxSDK


#import "SBSDK.h"

@import CoreLocation;

/**
 Utilities methods
 */
@interface SBSDK (Utilities)

///Simple check that phone number is in international format (starts with + sign).
+ (BOOL)isValidPhoneFormat:(nonnull NSString *)phone;

///Compute distance from user location to store
+ (CLLocationDistance)distanceFromUserLocation:(CLLocationCoordinate2D)location toStore:(nonnull SBStoreLocation *)store;

///Sort stores by distance to user location
+ (nonnull NSArray<SBStoreLocation *> *)sortStores:(nonnull NSArray<SBStoreLocation *> *)stores byDistanceToUserLocation:(CLLocationCoordinate2D)location;

@end
