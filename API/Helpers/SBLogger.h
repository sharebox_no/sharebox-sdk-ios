//
//  SBLogger.h
//  ShareboxSDK


#import <Foundation/Foundation.h>

//MARK: - HttpMethod
typedef NS_ENUM(NSUInteger, SBLogLevel) {
    SBLogLevelNone,
    SBLogLevelDebug,
};

NS_ASSUME_NONNULL_BEGIN

@interface SBLogger : NSObject
- (instancetype)init __attribute((unavailable("Not available")));

+ (void)logLevel:(SBLogLevel)logLevel;

+ (void)error:(NSError *)error;
+ (void)exception:(NSException *)exception;
+ (void)debug:(id)info;
@end

NS_ASSUME_NONNULL_END
