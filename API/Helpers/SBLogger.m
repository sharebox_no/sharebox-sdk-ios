//
//  SBLogger.m
//  ShareboxSDK


#import "SBLogger.h"
@interface SBLogger()
@property (nonatomic) SBLogLevel level;
@end

@implementation SBLogger

+(SBLogger *)shared {
    static SBLogger *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [SBLogger new];
        shared.level = SBLogLevelNone;
    });
    return shared;
}

+(void)logLevel:(SBLogLevel)logLevel {
    self.shared.level = logLevel;
}

+ (void)error:(NSError *)error {
    if (self.shared.level == SBLogLevelDebug) {
        NSLog(@"SBError: %@", error.description);
    }
};

+ (void)exception:(NSException *)exception {
    if (self.shared.level == SBLogLevelDebug) {
        NSLog(@"SBException: %@", exception.description);
    }
};

+ (void)debug:(id)info {
    if (self.shared.level == SBLogLevelDebug) {
        NSLog(@"SBDebug: %@", [info description]);
    }
};

@end
