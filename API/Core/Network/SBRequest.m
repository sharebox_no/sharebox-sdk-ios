//
//  SBRequest.m
//  ShareboxSDK


#import "SBRequest.h"
#import "NSString+SBNetwork.h"
#import "NSURLRequest+SBRequestable.h"
#import "NSError+SB.h"
#import "SBLogger.h"
#import "JSON+NSNullFree.h"

//MARK: - QueryPair
@implementation SBQueryPair
+(instancetype)pairWithKey:(NSString *)key value:(NSString *)value {
    SBQueryPair *instance = [self new];
    instance.key = key;
    instance.value = value;
    return instance;
}

- (NSString *)URLEncodedString {
    if (!self.value || [self.value isEqual:[NSNull null]]) {
        return self.key.description.queryEscaped;
    } else {
        return [NSString stringWithFormat:@"%@=%@", self.key.description.queryEscaped, self.value.description.queryEscaped];
    }
}
@end

//MARK: - Encoding
@implementation SBParameterEncoder
- (instancetype)initWithEncoding:(SBParameterEncoding)encoding {
    self = [super init];
    if (self) {
        _encoding = encoding;
    }
    return self;
}
+ (nonnull instancetype)encoderWithEncoding:(SBParameterEncoding)encoding {
    return [[SBParameterEncoder alloc] initWithEncoding:encoding];
}
+(instancetype)urlEncoder {
    return [SBParameterEncoder encoderWithEncoding:SBParameterEncodingUrl];
}
+(instancetype)jsonEncoder {
    return [SBParameterEncoder encoderWithEncoding:SBParameterEncodingJson];
}
- (NSString *)queryStringFromParameters:(NSDictionary *)paramters {
    NSMutableArray *mutablePairs = [NSMutableArray array];
    NSArray *components = [self queryComponentsWithKey:nil value:paramters];
    for (SBQueryPair *pair in components) {
        [mutablePairs addObject:[pair URLEncodedString]];
    }
    
    return [mutablePairs componentsJoinedByString:@"&"];
}

- (NSArray *)queryComponentsWithKey:(NSString *)key value:(id)value {
    NSMutableArray *mutableQueryStringComponents = [NSMutableArray array];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES selector:@selector(compare:)];
    
    if ([value isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dictionary = value;
        // Sort dictionary keys to ensure consistent ordering in query string, which is important when deserializing potentially ambiguous sequences, such as an array of dictionaries
        for (id nestedKey in [dictionary.allKeys sortedArrayUsingDescriptors:@[ sortDescriptor ]]) {
            id nestedValue = dictionary[nestedKey];
            if (nestedValue) {
                NSString *k = key ? [NSString stringWithFormat:@"%@[%@]", key, nestedKey] : nestedKey;
                [mutableQueryStringComponents addObjectsFromArray: [self queryComponentsWithKey:k value:nestedValue]];
            }
        }
    } else if ([value isKindOfClass:[NSArray class]]) {
        NSArray *array = value;
        for (id nestedValue in array) {
            [mutableQueryStringComponents addObjectsFromArray:[self queryComponentsWithKey:[NSString stringWithFormat:@"%@[]", key] value:nestedValue]];
        }
    } else if ([value isKindOfClass:[NSSet class]]) {
        NSSet *set = value;
        for (id obj in [set sortedArrayUsingDescriptors:@[ sortDescriptor ]]) {
            [mutableQueryStringComponents addObjectsFromArray:[self queryComponentsWithKey:key value:obj]];
        }
    } else {
        [mutableQueryStringComponents addObject:[SBQueryPair pairWithKey:key value:value]];
    }
    
    return mutableQueryStringComponents;
}



- (NSURLRequest *)encode:(id<SBRequestable>)request parameters:(NSDictionary *)parameters {
    NSMutableURLRequest *urlRequest = request.urlRequest.mutableCopy;
    SBHttpMethod method = urlRequest.HTTPMethod.httpMethod;
    switch (self.encoding) {
        case SBParameterEncodingUrl:
            if (method == SBHttpGet || method == SBHttpHead || method == SBHttpDelete) {
                NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:urlRequest.URL resolvingAgainstBaseURL:NO];
                if (urlComponents && parameters) {
                    NSString *percentEncoded = urlComponents.percentEncodedQuery;
                    if (percentEncoded) {
                        percentEncoded = [percentEncoded stringByAppendingString:@"&"];
                    } else {
                        percentEncoded = @"";
                    }
                    percentEncoded = [percentEncoded stringByAppendingString:[self queryStringFromParameters:parameters]];
                    urlComponents.percentEncodedQuery = percentEncoded;
                    urlRequest.URL = urlComponents.URL;
                }
            } else {
                //fall back to using application/x-www-form-urlencoded
                if (![urlRequest valueForHTTPHeaderField:@"Content-Type"]) {
                    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                }
                if (parameters) {
                    NSString *encodedQuery = [self queryStringFromParameters:parameters];
                    urlRequest.HTTPBody = [encodedQuery dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
                }
            }
            break;
        case SBParameterEncodingJson:
            if (parameters && [NSJSONSerialization isValidJSONObject:parameters]) {
                NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
                if (data) {
                    [urlRequest setValue: @"application/json" forHTTPHeaderField: @"Content-Type"];
                    urlRequest.HTTPBody = data;
                }
            }
            break;
    }
    return urlRequest;
}
@end

//MARK: - Serializer
@interface SBSerializer()
@property (nonatomic) SBSerializing serializing;
@property (nonatomic) NSStringEncoding stringEncoding;
@end

@implementation SBSerializer

+(instancetype)jsonSerializer {
    SBSerializer *instance = [self new];
    if (self) {
        instance.serializing = SBSerializingJson;
    }
    return instance;
}

+(instancetype)stringSerializerWithEncoding:(NSStringEncoding)encoding {
    SBSerializer *instance = [self new];
    if (self) {
        instance.serializing = SBSerializingString;
        instance.stringEncoding = encoding;
    }
    return instance;
}

- (id)serialize:(NSData *)data error:(NSError *__autoreleasing  _Nullable *)error {
    switch (self.serializing) {
        case SBSerializingString:
        {
            NSString *s = [[NSString alloc] initWithData:data encoding:self.stringEncoding];
            return s;
        }
            break;
        case SBSerializingJson:
        {
            id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
            if ([result isKindOfClass:[NSDictionary class]]) {
                result = [(NSDictionary *)result nsNullFree];
            } else if ([result isKindOfClass:[NSArray class]]) {
                result = [(NSArray *)result nsNullFree];
            } else {

            }
            return result;
        }
    }
    return data;
}

@end

//MARK: - Request
@interface SBRequest()
@property (nonatomic) dispatch_queue_t queue;
@property (nonatomic) NSData *data;
@property (nonatomic) NSError *error;
@property (nonatomic) NSURLSessionTask *task;
@property (nonatomic) NSURLSession *session;
@end

@implementation SBRequest
- (void)cancel {
    [self.task cancel];
}
- (void)resume {
    [self.task resume];
}
- (void)suspend {
    [self.task suspend];
}

- (BOOL)isCancelled {
    return (self.task.state == NSURLSessionTaskStateCanceling || self.task.state == NSURLSessionTaskStateCompleted);
}

- (instancetype)initWithSession:(NSURLSession *)session task:(NSURLSessionTask *)task {
    self = [super init];
    if (self) {
        _session = session;
        _task = task;
        NSString *label = [@"com.shareboxsdk.task-" stringByAppendingFormat:@"%lu", (unsigned long)task.taskIdentifier];
        _queue = dispatch_queue_create(label.UTF8String, DISPATCH_QUEUE_SERIAL);
        dispatch_suspend(_queue);
    }
    return self;
}

//MARK: Requestabl
- (NSURLRequest *)urlRequest {
    return self.task.originalRequest;
}

- (NSURLResponse *)urlResponse {
    return self.task.response;
}

- (NSHTTPURLResponse *)httpUrlResponse {
    if ([self.urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
        return (NSHTTPURLResponse *)self.urlResponse;
    } else {
        return nil;
    }
}

//MARK: NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    self.error = error;
    if (error) {
        self.data = nil;
    }
    dispatch_resume(_queue);
}
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    if (self.data) {
        NSMutableData *d = [NSMutableData dataWithData:self.data];
        [d appendData:data];
        self.data = d;
    } else {
        self.data = data;
    }
}
@end

//MARK: - Request + Response
@implementation SBRequest (Response)
- (SBRequest *)response:(SBRequestCompletion)completion {
    return [self responseWithSerializer:nil completion:completion];
}
//- (SBRequest *)reponse:(Class)modelClass completion:(SBRequestCompletion)completion {
//    return [self reponseWithSerializer:[SBSerializer modelClass:modelClass] completion:completion];
//}
//- (SBRequest *)reponseArray:(Class)modelClass completion:(SBRequestCompletion)completion {
//    return [self reponseWithSerializer:[SBSerializer modelClass:modelClass] completion:completion];
//}

- (SBRequest *)responseJson:(SBRequestCompletion)completion {
    return [self responseWithSerializer:SBSerializer.jsonSerializer completion:completion];
}
- (SBRequest *)responseString:(SBRequestCompletion)completion {
    return [self responseWithSerializer:[SBSerializer stringSerializerWithEncoding:NSUTF8StringEncoding] completion:completion];
}

- (SBRequest *)responseWithSerializer:(SBSerializer *)serializer completion:(SBRequestCompletion)completion {
    dispatch_async(self.queue, ^{
        if (self.data) {
            if (serializer) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSError *serializedError;
                    id result = [serializer serialize:self.data error:&serializedError];
                    NSError *error;
                    if ([result isKindOfClass:[NSDictionary class]]) {
                        error = [NSError SBErrorFromResponse:result]?:serializedError;
                    } else if (self.httpUrlResponse.statusCode == 401) {
                        error = [NSError SBUnauthorized];
                    } else {
                        error = serializedError;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            [SBLogger error:error];
                            completion(self, nil, error);
                        } else {
                            completion(self, result, serializedError);
                        }
                    });
                });
            } else {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSError *serializedError;
                    id result = [SBSerializer.jsonSerializer serialize:self.data error:&serializedError];
                    id error;
                    if ([result isKindOfClass:[NSDictionary class]]) {
                        error = [NSError SBErrorFromResponse:result]?:serializedError;
                    } else if (self.httpUrlResponse.statusCode == 401) {
                        error = [NSError SBUnauthorized];
                    } else {
                        error = serializedError;
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            [SBLogger error:error];
                            completion(self, nil, error);
                        } else {
                            completion(self, self.data, self.error);
                        }
                    });
                });
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.httpUrlResponse.statusCode == 401) {
                    [SBLogger error:[NSError SBUnauthorized]];
                    completion(self, nil, [NSError SBUnauthorized]);
                } else {
                    if (self.error) {
                        [SBLogger error:self.error];
                    }
                    if (self.error.code != NSURLErrorCancelled) {
                        completion(self, nil, self.error);
                    }
                }
            });
        }
    });
    return self;
}
@end
