//
//  SBRequest.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

NS_ASSUME_NONNULL_BEGIN

//MARK: - QueryPair
@interface SBQueryPair: NSObject
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;
+ (instancetype)pairWithKey:(NSString *)key value:(NSString *)value;
@end

//MARK: - Encoding
@interface SBParameterEncoder: NSObject
@property (nonatomic, readonly) SBParameterEncoding encoding;
- (instancetype)init __attribute((unavailable("Not available. Using initWithEncoding: instead")));
- (instancetype)initWithEncoding:(SBParameterEncoding)encoding NS_DESIGNATED_INITIALIZER;
+ (instancetype)encoderWithEncoding:(SBParameterEncoding)encoding;
+ (instancetype)urlEncoder;
+ (instancetype)jsonEncoder;
- (NSURLRequest *)encode:(id<SBRequestable>)request parameters:(nullable NSDictionary<NSString *, id> *)parameters;
@end

//MARK: - Serializer
@interface SBSerializer: NSObject
- (instancetype)init __attribute((unavailable("Not available. Using initWithClass: instead")));
+ (instancetype)jsonSerializer;
+ (instancetype)stringSerializerWithEncoding:(NSStringEncoding)encoding;

- (nullable id)serialize:(NSData*)data error:(NSError *_Nullable*_Nullable)error;
@end

//MARK: - Request
@interface SBRequest : NSObject <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, SBRequestable, SBCancelable>
@property (nonatomic, readonly) dispatch_queue_t queue;
@property (nullable, nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) NSURLSessionTask *task;
@property (nonatomic, readonly) NSURLSession *session;
@property (nonatomic, readonly) NSURLRequest *urlRequest;
@property (nullable, readonly) NSURLResponse *urlResponse;
@property (nullable, readonly) NSHTTPURLResponse *httpUrlResponse;
- (void)cancel;
- (void)resume;
- (void)suspend;
- (instancetype)init __attribute((unavailable("Not available. Using initWithSession:task: instead")));
- (instancetype)initWithSession:(NSURLSession *)session task:(NSURLSessionTask *)task NS_DESIGNATED_INITIALIZER;
- (BOOL)isCancelled;
@end

typedef void (^SBRequestCompletion)(SBRequest*_Nullable, id _Nullable , NSError*_Nullable);
@interface SBRequest (Response)
- (SBRequest *)response:(SBRequestCompletion)completion;
- (SBRequest *)responseJson:(SBRequestCompletion)completion;
- (SBRequest *)responseString:(SBRequestCompletion)completion;
- (SBRequest *)responseWithSerializer:(nullable SBSerializer *)serializer completion:(SBRequestCompletion)completion;
@end

NS_ASSUME_NONNULL_END
