//
//  NSError+SB.m
//  ShareboxSDK


#import "NSError+SB.h"

const NSString *SBApiResultCodeKey = @"result_code";
const NSString *SBApiErrorMessageKey = @"error_message";

@implementation NSError (SB)

+ (nullable instancetype)SBErrorFromResponse:(id)response {
    if (!response) {
        return nil;
    }
    if ([response isKindOfClass:[NSDictionary class]]) {
        NSNumber *code = response[SBApiResultCodeKey];
        NSString *msg = response[SBApiErrorMessageKey];
        if (code.integerValue != 0 && msg.length > 0) {
            NSDictionary *userInfo = @{SBApiResultCodeKey:code, SBApiErrorMessageKey:msg};
            return [NSError errorWithDomain:msg code:code.integerValue userInfo:userInfo];
        } else {
            return nil;
        }
    } else {
        //deal with Unauthorized etc errors
        return nil;
    }
}

+ (nonnull instancetype)SBUnauthorized {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [NSError errorWithDomain:@"com.sharebox.unauthorized" code:401 userInfo:nil];
    });
    return shared;
}
@end
