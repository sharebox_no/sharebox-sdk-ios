//
//  NSError+SB.h
//  ShareboxSDK


#import <Foundation/Foundation.h>

#define SB_API_ERROR_NOT_REGISTER 3
#define SB_API_ERROR_INVALID_SMS_CODE 401
#define SB_API_ERROR_TOO_FAR_FROM_LOCKER 105

@interface NSError (SB)
///parse error from response
+ (nullable instancetype)SBErrorFromResponse:(nonnull id)response;

///shared unauthorized error
+ (nonnull instancetype)SBUnauthorized;
@end
