//
//  SBApiClient+Endpoint.m
//  ShareboxSDK


#import "SBApiClient+Endpoint.h"
#import "SBCommon.h"
#import "NSString+SBNetwork.h"
#import "NSURLRequest+SBRequestable.h"
#import "NSError+SB.h"
#import "SBConsent.h"
#import "SBUserProfile.h"
#import "SBStoreLocation.h"
#import "SBReservation.h"

//MARK: - API Endpoints
static NSString *const SBApiGetConsent = @"/v2/sharebox_get_consent";
static NSString *const SBApiVerifyPhone = @"/v2/sharebox_authenticate_phone_number";
static NSString *const SBApiVerifySMS = @"/v2/sharebox_authenticate_sms_code";
static NSString *const SBApiLogin = @"/sharebox_login";
static NSString *const SBApiRegister = @"/v2/sharebox_register";
static NSString *const SBApiGetProfile = @"/sharebox_login";
static NSString *const SBApiGrantConsent = @"/v2/sharebox_accept_consent";
static NSString *const SBApiGetLocations = @"/v2/sharebox_get_locations";
static NSString *const SBApiGetLocationDetail = @"/v2/sharebox_get_location_details";
static NSString *const SBApiReserveLocker = @"/v2/sharebox_reserve_locker";
static NSString *const SBApiGetReservations = @"/v2/sharebox_get_active_reservations";
static NSString *const SBApiGetReservationsHistory = @"/v2/sharebox_get_nonactive_reservations";
static NSString *const SBApiGetReservationDetail = @"/v2/sharebox_get_reservation_details";
static NSString *const SBApiOpenLocker = @"/v2/sharebox_open_locker";
static NSString *const SBApiStopSubscription = @"/sharebox_subscription_stop";

@implementation SBApiClient (Endpoint)

//MARK: - internals
typedef void (^SBApiModelCompletion)(SBRequest *_Nonnull, id _Nullable, NSError *_Nullable);
typedef void (^SBApiArrayModelCompletion)(SBRequest *_Nonnull, NSArray *_Nullable, NSError *_Nullable);

- (SBRequest *)request:(id<SBRequestable>)rq modelClass:(Class)modelClass completion:(SBApiModelCompletion)completion {
    return [self request:rq modelClass:modelClass jsonKey:nil completion:completion];
}

- (SBRequest *)request:(id<SBRequestable>)rq modelClass:(Class)modelClass jsonKey:(NSString *)jsonKey completion:(SBApiModelCompletion)completion {
    if (![modelClass respondsToSelector:@selector(initWithJson:)]) {
        //fall back to non json initable
        //NSError *error = [NSError errorWithDomain:@"com.sharebox.model_not_jsoninitable" code:0 userInfo:nil];
        //completion(self, nil, error);
        NSAssert(false, @"model not jsoninitable");
    }
    
    return [self requestJson:rq completion:^(SBRequest * rq, id json, NSError *error) {
        id modelJson = json;
        if (jsonKey.length > 0) {
            modelJson = json[jsonKey];
        }
        if (modelJson) {
            id result = [modelClass initWithJson: modelJson];
            completion(rq, result, error);
        } else {
            completion(rq, nil, error);
        }
    }];
}

- (SBRequest *)requestArray:(id<SBRequestable>)rq modelClass:(Class)modelClass
                       jsonKey:(NSString *)jsonKey completion:(SBApiArrayModelCompletion)completion {
    if (![modelClass respondsToSelector:@selector(initWithJson:)]) {
        //fall back to non json initable
        //NSError *error = [NSError errorWithDomain:@"com.sharebox.model_not_jsoninitable" code:0 userInfo:nil];
        NSAssert(false, @"model not jsoninitable");
        //return [AnonymousCancelable new];
    }
    
    return [self requestJson:rq completion:^(SBRequest *rq, id json, NSError *error) {
        NSMutableArray *result = [NSMutableArray array];
        id modelJson = json;
        if (jsonKey.length > 0) {
            modelJson = json[jsonKey];
        }
        
        [modelJson enumerateObjectsUsingBlock:^(NSDictionary<NSString *,id> *json, NSUInteger idx, BOOL *stop) {
            id item = [modelClass initWithJson:json];
            [result addObject:item];
        }];
        completion(rq, result, error);
    }];
}


- (NSURLRequest *)buildRequestWithPath:(NSString *)path
                                method:(SBHttpMethod)method
                     parameterEncoding:(SBParameterEncoding)parameterEncoding
                            parameters:(NSDictionary *)parameters {
    NSURL *url = [self.configuration.baseUrl URLByAppendingPathComponent:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = [NSString stringFromHttpMethod:method];
    SBParameterEncoder *encoder = [SBParameterEncoder encoderWithEncoding:parameterEncoding];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (parameters.count > 0) {
        [params addEntriesFromDictionary:parameters];
    }
    
    NSURLRequest *finalRequest = [encoder encode:request parameters:params];
    return finalRequest;
}

- (NSDictionary *)paramsWithParams:(NSDictionary *)params sessionId:(NSString *)sessionId {
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:@{@"session_id" : sessionId}];
    if (params.count > 0) {
        [result addEntriesFromDictionary:params];
    }
    return result;
}

//MARK: - apis

- (SBRequest *)getConsentWithCompletion:(SBApiGetConsentCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetConsent
                                           method:SBHttpGet
                                parameterEncoding:SBParameterEncodingUrl
                                       parameters:nil];
    return [self request:rq modelClass:[SBConsent class] completion:completion];
}

- (SBRequest *)authenticatePhone:(SBApiParams)params completion:(SBApiEmptyCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiVerifyPhone
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:params];
    return [self requestJson:rq completion:^(SBRequest *rq, id json, NSError *error) {
        completion(rq, error);
    }];
}
- (SBRequest *)authenticateSMS:(SBApiParams)params completion:(SBApiJsonCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiVerifySMS
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:params];
    return [self requestJson:rq completion:completion];
}
- (SBRequest *)login:(SBApiParams)params completion:(SBApiLoginCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiLogin
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:params];
    return [self request:rq modelClass:[SBLoginInfo class] jsonKey:nil completion:completion];
}

- (SBRequest *)registerUser:(SBApiParams)params completion:(SBApiJsonCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiRegister
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:params];
    return [self requestJson:rq completion:completion];
}

- (SBRequest *)getProfileWithSessionId:(NSString *)sessionId completion:(SBApiGetProfileCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetProfile
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:nil sessionId:sessionId]];
    return [self request:rq modelClass:[SBUserProfile class] completion:completion];
}

- (SBRequest *)grantConsent:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGrantConsent
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self requestJson:rq completion:^(SBRequest *rq, id json, NSError *error) {
        completion(rq, error);
    }];
}

- (SBRequest *)getLocationsWithSessionId:(NSString *)sessionId completion:(SBApiGetLocationsCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetLocations
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:nil sessionId:sessionId]];
    return [self requestArray:rq modelClass:[SBStoreLocation class] jsonKey:@"locations" completion:completion];
}
- (SBRequest *)getLocationDetail:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiGetLocationDetailCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetLocationDetail
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self request:rq modelClass:[SBStoreLocation class] jsonKey:@"location" completion:completion];
}
- (SBRequest *)reserveLocker:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiReserveLockerCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiReserveLocker
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self request:rq modelClass:[SBReserveLockerInfo class] jsonKey:nil completion:completion];
}
- (SBRequest *)getReservationsWithSessionId:(NSString *)sessionId completion:(SBApiGetReservationsCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetReservations
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:nil sessionId:sessionId]];
    return [self requestArray:rq modelClass:[SBReservation class] jsonKey:@"reservations" completion:completion];
}
- (SBRequest *)getReservationsHistoryWithSessionId:(NSString *)sessionId completion:(SBApiGetReservationsCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetReservationsHistory
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:nil sessionId:sessionId]];
    return [self requestArray:rq modelClass:[SBReservation class] jsonKey:@"reservations" completion:completion];
}
- (SBRequest *)getReservationDetail:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiGetReservationDetailCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiGetReservationDetail
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self request:rq modelClass:[SBReservationInfo class] jsonKey:nil completion:completion];
}
- (SBRequest *)openLocker:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiOpenLocker
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self requestJson:rq completion:^(SBRequest *rq, id json, NSError *error) {
        completion(rq, error);
    }];
}
- (SBRequest *)stopSubscription:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion {
    NSURLRequest *rq = [self buildRequestWithPath:SBApiStopSubscription
                                           method:SBHttpPost
                                parameterEncoding:SBParameterEncodingJson
                                       parameters:[self paramsWithParams:params sessionId:sessionId]];
    return [self requestJson:rq completion:^(SBRequest *rq, id json, NSError *error) {
        completion(rq, error);
    }];
}
@end
