//
//  NSString+SBNetwork.h
//  ShareboxSDK
//
//  1/23/18.

#import <Foundation/Foundation.h>
#import "SBCommon.h"

@interface NSString (SBNetwork)
- (nonnull NSString *)queryEscaped;
- (SBHttpMethod)httpMethod;
+ (nonnull NSString *)stringFromHttpMethod:(SBHttpMethod)httpMethod;
@end
