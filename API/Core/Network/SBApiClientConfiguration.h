//
//  SBApiClientConfiguration.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBLogger.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBApiClientConfiguration : NSObject
@property (nonatomic, readonly) NSURL *baseUrl;
@property (nonatomic, readonly) NSString *basicAuthentication;
@property (nonatomic) SBLogLevel logLevel;

- (instancetype)init __attribute((unavailable("Not available. Using initWithSession:task: instead")));

/**
 Designed initialize method.

 @param baseUrl Api Base Url. If empty, fallback to default prod url ("https://prod.sharebox.no:3000")
 @param login login name for basic authentication
 @param password password for basic authentication
 @return instance of configuration
 */
- (instancetype)initWithBaseUrl:(nullable NSURL*)baseUrl
                          login:(NSString *)login
                       password:(NSString *)password
NS_DESIGNATED_INITIALIZER;


/**
 Search in the bundle the file with name "SBConfig.plist" and parse baseUrl and basic authentication login and password values from here.
 
 An exception will be thrown if the config file not found.

 @return an instance of configuration.
 */
+ (instancetype)defaultConfiguration;


/**
 Search in the bundle the file with path and parse baseUrl and basic authentication login and password values from here.

 An exception will be thrown if the config file not found.
 
 @param plist name of plist (with or without name extension '.plist'). If null or empty, return the default plist by search "SBConfig.plist".
 @return an instance of configuration.
 */
+ (instancetype)configurationWithPlist:(nullable NSString *)plist;
@end

NS_ASSUME_NONNULL_END
