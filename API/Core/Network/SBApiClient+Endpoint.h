//
//  SBApiClient+Endpoint.h
//  ShareboxSDK


#import "SBApiClient.h"
#import "SBRequest.h"
#import "SBResponseModels.h"

@class SBConsent;
@class SBStoreLocation;

NS_ASSUME_NONNULL_BEGIN

typedef NSDictionary<NSString *, id>* SBApiParams;

@interface SBApiClient (Endpoint)

///completion with error.
typedef void (^SBApiEmptyCompletion)(SBRequest *, NSError *_Nullable);
///json completion. Json is NSDictionary or NSArray.
typedef void (^SBApiJsonCompletion)(SBRequest *, id _Nullable , NSError *_Nullable);

///Get consent completion.
typedef void (^SBApiGetConsentCompletion)(SBRequest *, SBConsent *_Nullable, NSError *_Nullable);

///login completion.
typedef void (^SBApiLoginCompletion)(SBRequest *, SBLoginInfo *_Nullable, NSError *_Nullable);

///get user profile completion.
typedef void (^SBApiGetProfileCompletion)(SBRequest *, SBUserProfile *_Nullable, NSError *_Nullable);

///reserve locker completion.
typedef void (^SBApiReserveLockerCompletion)(SBRequest *, SBReserveLockerInfo *_Nullable, NSError *_Nullable);

///get locations completion.
typedef void (^SBApiGetLocationsCompletion)(SBRequest *, NSArray<SBStoreLocation *> *_Nullable, NSError *_Nullable);

///get location detail completion.
typedef void (^SBApiGetLocationDetailCompletion)(SBRequest *, SBStoreLocation *_Nullable, NSError *_Nullable);

///get reservations completion
typedef void (^SBApiGetReservationsCompletion)(SBRequest *, NSArray<SBReservation *> *_Nullable, NSError *_Nullable);

///get reservation detail completion
typedef void (^SBApiGetReservationDetailCompletion)(SBRequest *, SBReservationInfo *_Nullable, NSError *_Nullable);


- (SBRequest *)getConsentWithCompletion:(SBApiGetConsentCompletion)completion;
- (SBRequest *)authenticatePhone:(SBApiParams)params completion:(SBApiEmptyCompletion)completion;
- (SBRequest *)authenticateSMS:(SBApiParams)params completion:(SBApiJsonCompletion)completion;
- (SBRequest *)login:(SBApiParams)params completion:(SBApiLoginCompletion)completion;
- (SBRequest *)registerUser:(SBApiParams)params completion:(SBApiJsonCompletion)completion;
- (SBRequest *)getProfileWithSessionId:(NSString *)sessionId completion:(SBApiGetProfileCompletion)completion;
- (SBRequest *)grantConsent:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion;
- (SBRequest *)getLocationsWithSessionId:(NSString *)sessionId completion:(SBApiGetLocationsCompletion)completion;
- (SBRequest *)getLocationDetail:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiGetLocationDetailCompletion)completion;
- (SBRequest *)reserveLocker:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiReserveLockerCompletion)completion;
- (SBRequest *)getReservationsWithSessionId:(NSString *)sessionId completion:(SBApiGetReservationsCompletion)completion;
- (SBRequest *)getReservationsHistoryWithSessionId:(NSString *)sessionId completion:(SBApiGetReservationsCompletion)completion;
- (SBRequest *)getReservationDetail:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiGetReservationDetailCompletion) completion;
- (SBRequest *)openLocker:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion;
- (SBRequest *)stopSubscription:(SBApiParams)params sessionId:(NSString *)sessionId completion:(SBApiEmptyCompletion)completion;

@end

NS_ASSUME_NONNULL_END
