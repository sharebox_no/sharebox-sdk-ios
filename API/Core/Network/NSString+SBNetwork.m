//
//  NSString+SBNetwork.m
//  ShareboxSDK

#import "NSString+SBNetwork.h"

@implementation NSString (SBNetwork)

- (NSString *)queryEscaped {
    NSString *generalDelimitersToEncode = @":#[]@";
    NSString *subDelimitersToEncode = @"!$&'()*+,;=";
    NSString *removedCharacterString = [generalDelimitersToEncode stringByAppendingString:subDelimitersToEncode];
    NSMutableCharacterSet *allowedCharacterSet = [[NSCharacterSet URLQueryAllowedCharacterSet] mutableCopy];
    [allowedCharacterSet removeCharactersInString:removedCharacterString];
    return [self stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacterSet];
}

- (SBHttpMethod)httpMethod {
    if ([self isEqualToString:@"GET"]) {
        return SBHttpGet;
    } else if ([self isEqualToString:@"HEAD"]) {
        return SBHttpHead;
    } else if ([self isEqualToString:@"PUT"]) {
        return SBHttpPut;
    } else if ([self isEqualToString:@"POST"]) {
        return SBHttpPost;
    } else if ([self isEqualToString:@"PATCH"]) {
        return SBHttpPatch;
    } else if ([self isEqualToString:@"DELETE"]) {
        return SBHttpDelete;
    } else {
        return SBHttpGet;
    }
}

+ (NSString *)stringFromHttpMethod:(SBHttpMethod)httpMethod {
    switch (httpMethod) {
        case SBHttpGet: return @"GET";
        case SBHttpHead: return @"HEAD";
        case SBHttpPut: return @"PUT";
        case SBHttpPost: return @"POST";
        case SBHttpPatch: return @"PATCH";
        case SBHttpDelete: return @"DELETE";
    }
}

@end
