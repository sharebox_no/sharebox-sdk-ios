//
//  SBApiClient.h
//  Pods-ShareboxSDK_Example


#import <Foundation/Foundation.h>
#import "SBRequest.h"
#import "SBApiClientConfiguration.h"
#import "SBCommon.h"


/**
 One Api Client for whole Sharebox endpoints.
 
 Only shared default instance available.
 */

NS_ASSUME_NONNULL_BEGIN

@interface SBApiClient : NSObject

///Shared default client
+ (instancetype)defaultClient;
- (instancetype)init __attribute((unavailable("Not available. Using defaultClient instance instead")));
@property (nonatomic) SBApiClientConfiguration *configuration;
///Cancel all requests and reset the client.
- (void)reset;
@end

@interface SBApiClient (Request)
///Append request to operation list.
- (SBRequest *)request:(id<SBRequestable>)request;
///Append request with json data completion (SBRequest*, NSData* , NSError*)
- (SBRequest *)requestJson:(id<SBRequestable>)request completion:(SBRequestCompletion)completion;
@end

NS_ASSUME_NONNULL_END
