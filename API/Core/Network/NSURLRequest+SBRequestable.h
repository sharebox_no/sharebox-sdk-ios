//
//  NSURLRequest+Requestable.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

@interface NSURLRequest (SBRequestable) <SBRequestable>
- (nonnull NSURLRequest *)urlRequest;
@end
