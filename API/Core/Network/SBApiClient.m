//
//  SBApiClient.m
//  Pods-ShareboxSDK_Example


#import "SBApiClient.h"
#import "NSString+SBNetwork.h"
#import "SBCommon.h"
#import "NSURLRequest+SBRequestable.h"
#import "NSError+SB.h"

NSString * const SB_APICLIENT_OPERATION_QUEUE = @"com.sharebox.apiclient_operation_queue";

//MARK: - Declarations
@interface SBApiClientSessionDelegate : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>
@property (nonatomic, weak) SBApiClient *apiClient;
- (instancetype)initWithApiClient:(SBApiClient *)apiClient;
@end

@interface SBApiClient()
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) dispatch_queue_t operationQueue;
@property (nonatomic, strong) SBApiClientSessionDelegate *delegate;
@property (nonatomic, strong) NSMutableDictionary <NSNumber *, SBRequest *> *requests;
- (SBRequest *)requestWithTaskId:(NSInteger)taskId;
@end

//MARK: -SBApiClientSessionDelegate implementation
@implementation SBApiClientSessionDelegate
- (instancetype)initWithApiClient:(SBApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
    }
    return self;
}

//MARK: NSURLSessionDelegate
//MARK: NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest * _Nullable))completionHandler {
    completionHandler(request);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    SBRequest * rq = [self.apiClient requestWithTaskId:task.taskIdentifier];
    if (rq) {
        [rq URLSession:session task:task didCompleteWithError:error];
    }
}

//MARK: NSURLSessionDataDelegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    SBRequest * rq = [self.apiClient requestWithTaskId:dataTask.taskIdentifier];
    if (rq) {
        [rq URLSession:session dataTask:dataTask didReceiveData:data];
    }
}

//MARK: NSURLSessionDownloadDelegate
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
}

@end

//MARK: - Api Client implementation
@implementation SBApiClient
+ (instancetype)defaultClient {
    static id shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    SBApiClientSessionDelegate *delegate = [[SBApiClientSessionDelegate alloc] initWithApiClient:self];
    _delegate = delegate;
    _requests = [NSMutableDictionary dictionary];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    _session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self.delegate delegateQueue:nil];
    
    NSString *operationQueueName = SB_APICLIENT_OPERATION_QUEUE;
    _operationQueue = dispatch_queue_create(operationQueueName.UTF8String, DISPATCH_QUEUE_SERIAL);
}

//MARK: accessors
- (SBApiClientConfiguration *)configuration {
    if (_configuration) {
        return _configuration;
    } else {
        [NSException raise:@"Wrong Sharebox Configuration" format:@"No configuration found"];
    }
    return nil;
}
//MARK: internals
- (SBRequest *)requestWithTaskId:(NSInteger)taskId {
    __block SBRequest *request;
    dispatch_sync(self.operationQueue, ^{
        request = [self.requests objectForKey:@(taskId)];
    });
    return request;
}

- (void)addRequest:(SBRequest *)request {
    dispatch_sync(self.operationQueue, ^{
        self.requests[@(request.task.taskIdentifier)] = request;
    });
}

- (void)reset {
    dispatch_sync(self.operationQueue, ^{
        [self.requests enumerateKeysAndObjectsUsingBlock:^(NSNumber * _Nonnull key, SBRequest *  _Nonnull obj, BOOL * _Nonnull stop) {
            [obj cancel];
        }];
        [self.requests removeAllObjects];
    });
}

@end


//MARK: - Api Client + Request
@implementation SBApiClient (Request)

//MAKR: internals
- (SBRequest *)request:(id<SBRequestable>)request {
    __block NSURLSessionTask *task;
    __block SBRequest *rq;
    NSMutableURLRequest *urlRequest = [request.urlRequest mutableCopy];
    NSString *basicAuth = self.configuration.basicAuthentication;
    if (basicAuth.length > 0) {
        NSString *authorization = [@"Basic " stringByAppendingString:basicAuth];
        [urlRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
    }
    dispatch_sync(self.operationQueue, ^{
        task = [self.session dataTaskWithRequest:urlRequest];
    });
    rq = [[SBRequest alloc] initWithSession:self.session task:task];
    [self addRequest:rq];
    [rq resume];
    return rq;
}

- (SBRequest *)requestJson:(id<SBRequestable>)request completion:(SBRequestCompletion)completion {
    return [[self request:request] responseJson:completion];
}
@end
