//
//  SBApiClientConfiguration.m
//  ShareboxSDK


#import "SBApiClientConfiguration.h"

NSString * const SBApiClientProdBaseUrl = @"https://prod.sharebox.no:3000";

@interface SBApiClientConfiguration()
@property (nonatomic) NSURL *baseUrl;
@property (nonatomic) NSString *basicAuthentication;
@end

@implementation SBApiClientConfiguration

- (instancetype)initWithBaseUrl:(NSURL *)baseUrl
                          login:(NSString *)login
                       password:(NSString *)password {
    self = [super init];
    if (self) {
        if (baseUrl.absoluteString.length > 0) {
            _baseUrl = baseUrl;
        } else {
            _baseUrl = [NSURL URLWithString:SBApiClientProdBaseUrl];
        }
        
        if (login.length > 0 && password.length > 0) {
            NSString *auth = [NSString stringWithFormat:@"%@:%@", login, password];
            NSData *authData = [auth dataUsingEncoding:NSUTF8StringEncoding];
            _basicAuthentication = [authData base64EncodedStringWithOptions:0];
            _logLevel = SBLogLevelNone;
        } else {
            [NSException raise:@"Wrong Sharebox Configuration" format:@"Basic Authentication login or password is empty"];
        }
    }
    return self;
}

+ (nonnull instancetype)defaultConfiguration {
    return [self configurationWithPlist:@"SBConfig.plist"];
}

+ (nonnull instancetype)configurationWithPlist:(NSString *)plist {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:plist ofType:@"plist"];
    if ([plist.pathExtension isEqualToString: @"plist"]) {
        path = [bundle pathForResource:plist ofType:nil];
    }
    if (path.length == 0) {
        [NSException raise:@"Wrong Sharebox Configuration" format:@"Can not find the configuration plist with name: %@", plist];
    }
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    
    NSURL *baseUrl = [NSURL URLWithString:dictionary[@"BaseUrl"]];
    NSString *login = dictionary[@"Login"];
    NSString *password = dictionary[@"Password"];
    
    SBApiClientConfiguration *instance = [[SBApiClientConfiguration alloc] initWithBaseUrl:baseUrl login:login password:password];
    return instance;
}

- (NSString *)basicAuthentication {
    if (_basicAuthentication) {
        return _basicAuthentication;
    } else {
        [NSException raise:@"Wrong Sharebox Configuration" format:@"Basic Authentication login or password is empty"];
    }
    return nil;
}
@end
