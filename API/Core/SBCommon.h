//
//  SB.h
//  ShareboxSDK


#ifndef SB_h
#define SB_h

//MARK: - Requestable
@protocol SBRequestable
- (nonnull NSURLRequest *)urlRequest;
@end

//MARK: - JsonInstanable
@protocol SBJsonInstanable
+ (nonnull instancetype)initWithJson:(nonnull NSDictionary<NSString *, id> *)json;
@end

//MARK: - Cancelable
@protocol SBCancelable
- (void)cancel;
- (BOOL)isCancelled;
@end

//MARK: - JsonCodable
@protocol SBJsonCodeable
- (nonnull NSDictionary<NSString *, id> *)json;
@end

//MARK: - HttpMethod
typedef NS_ENUM(NSUInteger, SBHttpMethod) {
    SBHttpGet,
    SBHttpHead,
    SBHttpPost,
    SBHttpPut,
    SBHttpPatch,
    SBHttpDelete
};

//MARK: - ParameterEncoding
typedef NS_ENUM(NSUInteger, SBParameterEncoding) {
    SBParameterEncodingUrl,
    SBParameterEncodingJson
};

//MARK: - Serializer
typedef NS_ENUM(NSUInteger, SBSerializing) {
    SBSerializingString,
    SBSerializingJson
};

//MARK: - Endpoint
typedef NS_ENUM(NSUInteger, SBEndpoint) {
    SBEndpointLogin
};

//MARK: - Error
typedef NS_ENUM(NSUInteger, SBError) {
    SBErrorUnknown,
    SBErrorJsonFormat
};

//MARK: - Datetime format
#define SBDateFormat @"yyyy-MM-dd'T'HH:mm:ss.sZ"

//MARK: - strongify and weakify
#define SBWeakify(var) __weak typeof(var) SBWeak_##var = var;

#define SBStrongify(var) \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__strong typeof(var) var = SBWeak_##var; \
_Pragma("clang diagnostic pop")

#endif /* SB_h */
