//
//  SBResponseModels.m
//  ShareboxSDK


#import "SBResponseModels.h"
#import "SBUserProfile.h"
#import "SBStoreLocation.h"
#import "SBReservation.h"
#import "SBReservationEvent.h"
#import "SBReservationUser.h"

@implementation SBLoginInfo
+ (instancetype)initWithJson:(NSDictionary<NSString *,id> *)json {
    SBLoginInfo *instance = [SBLoginInfo new];
    instance.sessionId = json[@"session_id"];
    instance.userProfile = [SBUserProfile initWithJson:json];
    return instance;
}
@end

@implementation SBReserveLockerInfo
+ (instancetype)initWithJson:(NSDictionary<NSString *,id> *)json {
    SBReserveLockerInfo *instance = [SBReserveLockerInfo new];
    instance.reservationId = [json[@"reservation_id"] integerValue];
    return instance;
}
@end

@implementation SBReservationInfo
+ (instancetype)initWithJson:(NSDictionary<NSString *,id> *)json {
    SBReservationInfo *instance = [SBReservationInfo new];
    
    NSDictionary *detailJson = json[@"details"];
    NSDictionary *locationJson = json[@"location"];
    NSArray *eventsJson = json[@"events"];
    NSArray *membersJson = json[@"members"];
    
    SBReservation *reservation = [SBReservation initWithJson: detailJson];
    SBStoreLocation *location = [SBStoreLocation initWithJson:locationJson];
    reservation.location = location;
    __block NSMutableArray *events = [NSMutableArray array];
    [eventsJson enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        SBReservationEvent *event = [SBReservationEvent initWithJson: obj];
        [events addObject:event];
    }];
    
    __block NSMutableArray *members = [NSMutableArray array];
    [membersJson enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        SBReservationUser *user = [SBReservationUser initWithJson: obj];
        [members addObject:user];
    }];
    reservation.recipients = members;
    
    instance.reservation = reservation;
    instance.events = events;
    return instance;
}
@end
