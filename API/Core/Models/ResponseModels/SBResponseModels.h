//
//  SBResponseModels.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

@class SBUserProfile;
@class SBReservation;
@class SBReservationEvent;

//MARK: - helper classes. Support non-standard data

NS_ASSUME_NONNULL_BEGIN

@interface SBLoginInfo: NSObject <SBJsonInstanable>
@property (nonatomic) NSString *sessionId;
@property (nonatomic) SBUserProfile *userProfile;

+ (instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

@interface SBReserveLockerInfo: NSObject <SBJsonInstanable>
@property (nonatomic) NSInteger reservationId;

+ (instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

@interface SBReservationInfo: NSObject <SBJsonInstanable>
@property (nonatomic) SBReservation *reservation;
@property (nonatomic) NSArray<SBReservationEvent *> *events;

+ (instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
