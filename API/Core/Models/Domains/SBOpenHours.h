//
//  SBOpenHours.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBOpenHours : NSObject <SBJsonInstanable>
@property(nonatomic) NSInteger weekday;
@property(nonatomic) NSInteger startHour;
@property(nonatomic) NSInteger startMinute;
@property(nonatomic) NSInteger endHour;
@property(nonatomic) NSInteger endMinute;

- (BOOL)is24Hours;
- (NSString *)timeRangeString;
//JsonInitable
+(instancetype )initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
