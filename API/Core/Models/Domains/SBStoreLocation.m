//
//  SBStoreLocation.m
//  ShareboxSDK


#import "SBStoreLocation.h"
#import "SBCabinet.h"
#import "SBOpenHours.h"

@implementation SBStoreLocation

- (NSString *)fullAddress {
    NSMutableArray *array = [NSMutableArray array];
    if (self.address.length > 0) {
        [array addObject:self.address];
    }
    if (self.zipcode.length > 0 && self.city.length > 0) {
        [array addObject:[self.zipcode stringByAppendingFormat:@" %@", self.city]];
    } else if (self.zipcode.length > 0) {
        [array addObject:self.zipcode];
    } else if (self.city.length > 0) {
        [array addObject:self.city];
    } else {
        
    }
    if (self.country.length > 0) {
        [array addObject:self.country];
    }
    
    return [array componentsJoinedByString:@", "];
}

- (SBCabinet *)firstAvailableCarbinet {
    __block SBCabinet *cabinet;
    [self.cabinets enumerateObjectsUsingBlock:^(SBCabinet *obj, NSUInteger idx, BOOL *stop) {
        if (obj.isOnline && obj.isHasFreeLockers) {
            cabinet = obj;
            *stop = YES;
        }
    }];
    return cabinet;
}

- (SBOpenHours *)findOpenHoursForWeekday:(NSInteger)weekday {
    __block SBOpenHours *openHour;
    [self.openHours enumerateObjectsUsingBlock:^(SBOpenHours *obj, NSUInteger idx, BOOL *stop) {
        if (obj.weekday == weekday) {
            openHour = obj;
            *stop = YES;
        }
    }];
    return openHour;
}

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBStoreLocation *instance = [SBStoreLocation new];
    instance.identifier = [json[@"location_id"] integerValue];
    
    NSArray *cabinetsJson = json[@"cabinets"];
    __block NSMutableArray *cabinets = [NSMutableArray array];
    [cabinetsJson enumerateObjectsUsingBlock:^(id cabinetJson, NSUInteger idx, BOOL *stop) {
        SBCabinet *cabinet = [SBCabinet initWithJson:cabinetJson];
        [cabinets addObject:cabinet];
    }];
    instance.cabinets = cabinets;
    
    instance.latitude = json[@"latitude"];
    instance.longitude = json[@"longitude"];
    instance.name = json[@"name"];
    instance.address = json[@"address"];
    instance.city = json[@"city"];
    instance.zipcode = json[@"zipcode"];
    instance.country = json[@"country"];
    instance.chainId = [json[@"store_chain_id"] integerValue];
    
    NSArray *openHoursJson = json[@"opening_hours"];
    __block NSMutableArray *openHours = [NSMutableArray array];
    [openHoursJson enumerateObjectsUsingBlock:^(id openHourJson, NSUInteger idx, BOOL *stop) {
        SBOpenHours *openHour = [SBOpenHours initWithJson:openHourJson];
        [openHours addObject:openHour];
    }];
    instance.openHours = openHours;
    return instance;
}
@end
