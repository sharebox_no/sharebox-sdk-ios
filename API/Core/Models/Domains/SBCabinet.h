//
//  SBCabinet.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBCabinet : NSObject <SBJsonInstanable>
@property(nonatomic) NSInteger identifier;
@property(nonatomic) BOOL isOnline;
@property(nonatomic) BOOL isHasFreeLockers;
//JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
