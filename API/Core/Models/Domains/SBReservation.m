//
//  SBReservation.m
//  ShareboxSDK


#import "SBReservation.h"
#import "NSString+SBUtilities.h"
#import "SBStoreLocation.h"
#import "SBReservationUser.h"

@implementation SBReservation

- (BOOL)canOpen {
    return self.isActive;
}

- (BOOL)canFinish {
    return (self.isFromYou && self.type != SBReservationTypeMonthly);
}

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBReservation *instance = [SBReservation new];
    instance.identifier = [json[@"id"] integerValue];
    
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = SBDateFormat;
    df.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    instance.date = [df dateFromString:json[@"date"]];

    instance.subscriptionId = [json[@"subscription_id"] integerValue];
    instance.name = json[@"subscription_name"];
    instance.isFromYou = [json[@"reserved_by_you"] boolValue];
    
    SBReservationUser *owner = [SBReservationUser new];
    owner.name = [NSString nameFromFirstName:json[@"reserver_firstname"] lastName:json[@"reserver_lastname"]];
    owner.phone = json[@"reserver_mobile"];
    instance.owner = owner;
    
    instance.recipientsCount = [json[@"receivers_count"] integerValue];
    
    id firstRecipientJson = json[@"receiver_first"];
    if (firstRecipientJson) {
        instance.recipients = @[[SBReservationUser initWithJson:firstRecipientJson]];
    } else {
        instance.recipients = [NSArray array];
    }
    
    id locationJson = json[@"location"];
    if (locationJson) {
        instance.location = [SBStoreLocation initWithJson:locationJson];
    } else {
        instance.location = nil;
    }
    instance.lockerId = [json[@"locker_id"] integerValue];
    instance.lockerNumber = [json[@"locker_no"] integerValue];
    
    NSInteger type = [json[@"reservation_type_id"] integerValue];
    NSInteger isFree = [json[@"is_free"] boolValue];
    
    if (type == 2) {
        instance.type = SBReservationTypeMonthly;
    } else if (type == 3) {
        instance.type = isFree ? SBReservationTypeFree : SBReservationTypeRegular;
    } else {
        instance.type = SBReservationTypeUnknown;
    }
    
    instance.isActive = ([json[@"status"] integerValue] == 14);
    instance.isOpen = [json[@"is_locker_open"] boolValue];
    instance.freeHours = [json[@"free_hours"] integerValue];
    return instance;
}

@end
