//
//  SBConsent.m
//  ShareboxSDK


#import "SBConsent.h"

@implementation SBConsent

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBConsent *instance = [SBConsent new];
    instance.versionCode = [json[@"version_code"] integerValue];
    instance.versionName = json[@"version"];
    instance.content = json[@"content"];
    return instance;
}
//JsonCodable
- (NSDictionary<NSString *, id> *)json {
    NSMutableDictionary *json = [NSMutableDictionary dictionary];
    json[@"version_code"] = @(self.versionCode);
    if (self.versionName) {
        json[@"version"] = self.versionName;
    }
    if (self.content) {
        json[@"content"] = self.content;
    }
    return json;
}
@end
