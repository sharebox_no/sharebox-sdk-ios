//
//  SBStoreLocation.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"
@class SBCabinet;
@class SBOpenHours;

NS_ASSUME_NONNULL_BEGIN

@interface SBStoreLocation : NSObject <SBJsonInstanable>
@property(nonatomic) NSInteger identifier;
@property(nonatomic) NSArray<SBCabinet *> *cabinets;
@property(nonatomic) NSNumber *latitude;
@property(nonatomic) NSNumber *longitude;
@property(nonatomic) NSString *name;
@property(nullable, nonatomic) NSString *address;
@property(nullable, nonatomic) NSString *city;
@property(nullable, nonatomic) NSString *zipcode;
@property(nullable, nonatomic) NSString *country;
@property(nonatomic) NSInteger chainId;
@property(nonatomic) NSArray<SBOpenHours *> *openHours;

- (nullable NSString *)fullAddress;
- (nullable SBCabinet *)firstAvailableCarbinet;
- (nullable SBOpenHours *)findOpenHoursForWeekday:(NSInteger)weekday;

//JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
