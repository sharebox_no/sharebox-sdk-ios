//
//  SBReservationUser.m
//  ShareboxSDK


#import "SBReservationUser.h"

@implementation SBReservationUser
//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBReservationUser *instance = [SBReservationUser new];
    if ([json[@"name"] isKindOfClass:[NSString class]]) {
        instance.name = json[@"name"];
    } else {
        instance.name = nil;
    }
    
    if ([json[@"mobile"] isKindOfClass:[NSString class]]) {
        instance.phone = json[@"mobile"];
    } else {
        instance.phone = nil;
    }
    
    return instance;
}
@end
