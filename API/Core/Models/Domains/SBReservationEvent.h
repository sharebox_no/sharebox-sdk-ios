//
//  SBReservationEvent.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"
@class SBReservationUser;

typedef NS_ENUM(NSUInteger, SBReservationEventType) {
    SBReservationEventTypeCreated,
    SBReservationEventTypeOpened,
    SBReservationEventTypeUnknown
};

NS_ASSUME_NONNULL_BEGIN

@interface SBReservationEvent : NSObject <SBJsonInstanable>
@property(nonatomic) SBReservationEventType type;
@property(nonatomic) SBReservationUser *user;
@property(nonatomic) NSDate *date;
@property(nonatomic) BOOL isByYou;

//JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
