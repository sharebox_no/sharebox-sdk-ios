//
//  SBCabinet.m
//  ShareboxSDK


#import "SBCabinet.h"

@implementation SBCabinet

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBCabinet *instance = [SBCabinet new];
    instance.identifier = [json[@"cabinet_id"] integerValue];
    instance.isOnline = [json[@"online"] boolValue];
    instance.isHasFreeLockers = [json[@"free_lockers"] integerValue] > 0;
    return instance;
}
@end
