//
//  SBOpenHours.m
//  ShareboxSDK


#import "SBOpenHours.h"

@implementation SBOpenHours

- (BOOL)is24Hours {
    return (self.startHour == self.endHour) && (self.startMinute == self.endMinute);
}

- (NSString *)timeRangeString {
    return [NSString stringWithFormat:@"%02ld:%02ld - %02ld:%02ld", (long)self.startHour, (long)self.startMinute, (long)self.endHour, (long)self.endMinute];
}

+ (NSInteger)weekdayFromString:(NSString *)string {
    //force to use NSGregorianCalendar since api should return 'mon', 'tue', 'wed' etc
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //force to use en_US_POSIX locale to align format with api response
    [gregorian setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    
    __block NSInteger weekday = gregorian.firstWeekday;
    [[gregorian shortWeekdaySymbols] enumerateObjectsUsingBlock:^(NSString *weekdayString, NSUInteger idx, BOOL *stop) {
        if ([weekdayString.lowercaseString isEqualToString:string.lowercaseString]) {
            weekday = idx + 1; //start is Sun with value 1.
            *stop = YES;
        }
    }];
    return weekday;
}

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBOpenHours *instance = [SBOpenHours new];
    instance.weekday = [self weekdayFromString:json[@"day_of_week"]];
    NSArray *openHourComponents = [json[@"opening_hour"] componentsSeparatedByString:@":"];
    instance.startHour = [openHourComponents[0] integerValue];
    instance.startMinute = [openHourComponents[1] integerValue];
    NSArray *endHourComponents = [json[@"closing_hour"] componentsSeparatedByString:@":"];
    instance.endHour = [endHourComponents[0] integerValue];
    instance.endMinute = [endHourComponents[1] integerValue];
    return instance;
}
@end
