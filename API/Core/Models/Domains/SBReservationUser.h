//
//  SBReservationUser.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBReservationUser : NSObject <SBJsonInstanable>
@property(nullable, nonatomic) NSString *phone;
@property(nullable, nonatomic) NSString *name;
//JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
