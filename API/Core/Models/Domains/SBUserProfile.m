//
//  SBUserProfile.m
//  ShareboxSDK


#import "SBUserProfile.h"
#import "NSString+SBUtilities.h"

@implementation SBUserProfile

- (NSString *)fullName {
    return [NSString nameFromFirstName:self.firstName lastName:self.lastName];
}

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBUserProfile *instance = [SBUserProfile new];
    instance.firstName = json[@"first_name"];
    instance.lastName = json[@"last_name"];
    instance.email = json[@"email"];
    instance.phone = json[@"mobile"];
    if ([json[@"customer_status"] integerValue] == SBUserStatusBlocked) {
        instance.status = SBUserStatusBlocked;
    } else {
        instance.status = SBUserStatusOk;
    }
    instance.consentGranted = ![json[@"must_provide_consent"] boolValue];
    return instance;
}

//JsonCodable
- (NSDictionary<NSString *, id> *)json {
    NSMutableDictionary *json = [NSMutableDictionary dictionary];
    if (self.firstName) {
        json[@"first_name"] = self.firstName;
    }
    if (self.lastName) {
        json[@"last_name"] = self.lastName;
    }
    if (self.email) {
        json[@"email"] = self.email;
    }
    if (self.phone) {
        json[@"mobile"] = self.phone;
    }
    json[@"customer_status"] = @(self.status);
    json[@"must_provide_consent"] = @(!self.consentGranted);
    return json;
}
@end
