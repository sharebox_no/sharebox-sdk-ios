//
//  SBReservationEvent.m
//  ShareboxSDK


#import "SBReservationEvent.h"
#import "NSString+SBUtilities.h"
#import "SBReservationUser.h"

@implementation SBReservationEvent

//MARK: - JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json {
    SBReservationEvent *instance = [SBReservationEvent new];
    NSInteger type = [json[@"event_type_id"] integerValue];
    if (type == 6 || type == 18) {
        instance.type = SBReservationEventTypeCreated;
    } else if (type == 7 || type == 500) {
        instance.type = SBReservationEventTypeOpened;
    } else {
        instance.type = SBReservationEventTypeUnknown;
    }
    
    SBReservationUser *user = [SBReservationUser new];
    user.name = [NSString nameFromFirstName:json[@"firstname"] lastName:json[@"lastname"]];
    user.phone = json[@"mobile"];
    instance.user = user;
    
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = SBDateFormat;
    df.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    instance.date = [df dateFromString:json[@"registered"]];
    instance.isByYou = [json[@"opened_by_you"] boolValue];
    return instance;
}

@end
