//
//  SBUserProfile.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

typedef NS_ENUM(NSUInteger, SBUserStatus) {
    SBUserStatusOk,
    SBUserStatusBlocked = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface SBUserProfile : NSObject <SBJsonInstanable, SBJsonCodeable>

@property(nullable, nonatomic) NSString *firstName;
@property(nullable, nonatomic) NSString *lastName;
@property(nullable, nonatomic) NSString *email;
@property(nullable, nonatomic) NSString *phone;
@property(nonatomic) SBUserStatus status;
@property(nonatomic) BOOL consentGranted;

- (nullable NSString *)fullName;
//JsonInitable
+ (instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
//JsonCodable
- (nonnull NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
