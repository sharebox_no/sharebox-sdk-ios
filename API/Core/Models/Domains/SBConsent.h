//
//  SBConsent.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

NS_ASSUME_NONNULL_BEGIN
@interface SBConsent : NSObject <SBJsonInstanable, SBJsonCodeable>

@property(nonatomic) NSInteger versionCode;
@property(nullable, nonatomic) NSString *versionName;
@property(nullable, nonatomic) NSString *content;

//JsonInitable
+ (instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
//JsonCodable
- (NSDictionary<NSString *, id> *)json;
@end
NS_ASSUME_NONNULL_END
