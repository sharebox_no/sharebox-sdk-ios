//
//  SBReservation.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import "SBCommon.h"

@class SBStoreLocation;
@class SBReservationUser;

//MARK: - HttpMethod
typedef NS_ENUM(NSUInteger, SBReservationType) {
    SBReservationTypeUnknown,
    SBReservationTypeFree,
    SBReservationTypeRegular,
    SBReservationTypeMonthly
};

NS_ASSUME_NONNULL_BEGIN

@interface SBReservation : NSObject <SBJsonInstanable>
@property(nonatomic) NSInteger identifier;
@property(nonatomic) NSDate *date;
@property(nonatomic) NSInteger subscriptionId;
@property(nullable, nonatomic) NSString *name;
@property(nonatomic) BOOL isFromYou;
@property(nonatomic) SBReservationUser *owner;
@property(nonatomic) NSArray<SBReservationUser *> *recipients;
@property(nonatomic) NSInteger recipientsCount;
@property(nullable, nonatomic) SBStoreLocation *location;
@property(nonatomic) NSInteger lockerId;
@property(nonatomic) NSInteger lockerNumber;
@property(nonatomic) SBReservationType type;
@property(nonatomic) BOOL isActive;
@property(nonatomic) BOOL isOpen;
@property(nonatomic) NSInteger freeHours;

- (BOOL)canOpen;
- (BOOL)canFinish;

//JsonInitable
+(instancetype)initWithJson:(NSDictionary<NSString *, id> *)json;
@end

NS_ASSUME_NONNULL_END
