//
//  NSString+SBUtilities.m
//  ShareboxSDK


#import "NSString+SBUtilities.h"

@implementation NSString (SBUtilities)
+ (NSString *)nameFromFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    NSString *name;
    
    NSString *first = nil;
    NSString *last = nil;
    if ([firstName isKindOfClass:[NSString class]]) {
        first = firstName;
    }
    if ([lastName isKindOfClass:[NSString class]]) {
        last = lastName;
    }
    
    if (first && last) {
        name = [first stringByAppendingFormat:@" %@", last];
    } else if (first) {
        name = first;
    } else if (last) {
        name = last;
    } else {
        
    }
    
    return name;
}
@end
