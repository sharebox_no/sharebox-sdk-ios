//
//  JSON+NSNullFree.m
//  ShareboxSDK
//

#import "JSON+NSNullFree.h"

@implementation NSDictionary(NSNullFree)
- (NSDictionary *)nsNullFree {
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:self];
    [self enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            replaced[key] = [(NSDictionary *)obj nsNullFree];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            replaced[key] = [(NSArray *)obj nsNullFree];
        } else if ([obj isEqual:[NSNull null]]) {
            replaced[key] = nil;
        } else {
            //keep old value
        }
    }];
    return replaced;
}
@end

@implementation NSArray(NSNullFree)
- (NSArray *)nsNullFree {
    NSMutableArray *replaced = [NSMutableArray arrayWithArray:self];
    [self enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            replaced[idx] = [(NSDictionary *)obj nsNullFree];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            replaced[idx] = [(NSArray *)obj nsNullFree];
        } else if ([obj isEqual:[NSNull null]]) {
            [replaced removeObjectAtIndex:idx];
        } else {
            //keep old value
        }
    }];
    return replaced;
}
@end
