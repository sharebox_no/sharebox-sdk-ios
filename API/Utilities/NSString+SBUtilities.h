//
//  NSString+SBUtilities.h
//  ShareboxSDK


#import <Foundation/Foundation.h>

@interface NSString (SBUtilities)
+ (nullable NSString *)nameFromFirstName:(nullable NSString *)firstName lastName:(nullable NSString *)lastName;
@end
