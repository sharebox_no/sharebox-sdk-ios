//
//  JSON+NSNullFree.h
//  ShareboxSDK

NS_ASSUME_NONNULL_BEGIN
@interface NSDictionary(NSNullFree)

/**
 Enumerate through and remove NSNull items.

 @return new dictionary with NSNull free.
 */
- (NSDictionary *)nsNullFree;
@end

@interface NSArray(NSNullFree)
/**
 Enumerate through and remove NSNull items.
 
 @return new array with NSNull free.
 */
- (NSArray *)nsNullFree;
@end
NS_ASSUME_NONNULL_END
