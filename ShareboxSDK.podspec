#
# Be sure to run `pod lib lint ShareboxSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ShareboxSDK'
  s.version          = '1.0.3'
  s.summary          = 'Allows app developers to integrate Sharebox® service into their native iOS apps.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Sharebox simplifies your handover by offering a service completely managed by your phone.
App-controlled cabinets, placed at central locations, ensure you a safe and efficient handover
whenever you need.

Sharebox SDK provides functionality to book lockers for free for a limited period of time.
Users can book any lockers they want, however each free reservation will be converted into a paid
one once free period ends
                       DESC

  s.homepage         = 'https://bitbucket.org/sharebox_no/sharebox-sdk-ios/overview'
  s.license          = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author           = { 'Sharebox AS' => 'post@sharebox.no' }
  s.source           = { :git => 'https://bitbucket.org/sharebox_no/sharebox-sdk-ios.git', :tag => s.version.to_s }

  s.default_subspec = 'SDK'
  s.ios.deployment_target = '9.0'
  
  s.subspec 'API' do |ss|
      ss.source_files = 'API/**/*'
  end
  
  s.subspec 'SDK' do |ss|
      ss.source_files = 'SDK/**/*'
      ss.dependency 'ShareboxSDK/API'
      ss.ios.frameworks = 'Security', 'CoreLocation'
  end

end
