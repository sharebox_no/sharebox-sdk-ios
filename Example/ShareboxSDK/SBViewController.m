//
//  SBViewController.m
//  ShareboxSDK


#import "SBViewController.h"
@import ShareboxSDK;
#import "SBUI.h"
#import "UIViewController+Alert.h"

@interface SBViewController ()

@end

@implementation SBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openSharebox:(id)sender {
    UIViewController *vc = [SBUI entryViewController];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
