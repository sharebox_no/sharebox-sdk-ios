//
//  SBAppDelegate.h
//  ShareboxSDK


@import UIKit;

@interface SBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
