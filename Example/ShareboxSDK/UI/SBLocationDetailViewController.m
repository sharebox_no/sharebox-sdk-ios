//
//  SBLocationDetailViewController.m
//  ShareboxSDK


#import "SBLocationDetailViewController.h"
#import "UIViewController+Alert.h"
@import ShareboxSDK;
@import MapKit;
#import "SBUI.h"

@interface SBLocationDetailViewController ()
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *openHoursLabel;
@property (nonatomic, weak) IBOutlet UITextField *reservationNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UIButton *reserveButton;
@end

@implementation SBLocationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadUI];
    [self loadLocation];
}

//MARK: - actions
- (IBAction)reserveLocker:(id)sender {
    NSString *phone = self.phoneTextField.text;
    NSString *reservationName = self.reservationNameTextField.text;
    BOOL isValid = [SBSDK isValidPhoneFormat:phone] && reservationName.length > 0;
    if (!isValid) {
        return;
    }
    SBCabinet *cabinet = self.location.firstAvailableCarbinet;
    if (!cabinet) {
        return;
    }
    
    SBWeakify(self);
    
    [SBSDK.shared reserveLockerWithCabinetId:cabinet.identifier receiverPhones:@[phone] reservationName:reservationName completion:^(SBReserveLockerInfo *reservationInfo, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            //
            [self openReservationList];
        }
    }];
}

//MARK: - internals
- (void)reloadUI {
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.location.latitude.doubleValue, self.location.longitude.doubleValue);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.005, 0.005));
    //[self.mapView setCenterCoordinate:coordinate animated:YES];
    [self.mapView setRegion:region animated:YES];
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    //[annotation setTitle:@"Title"];
    //[annotation setSubtitle:@"sub title"];
    [self.mapView addAnnotation:annotation];
    
    self.nameLabel.text = self.location.name;
    self.addressLabel.text = self.location.address;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    SBOpenHours *openHours = [self.location findOpenHoursForWeekday:dateComponents.weekday];
    if (openHours) {
        self.openHoursLabel.text = openHours.is24Hours ? @"Open 24 hours" : openHours.timeRangeString;
    } else {
        self.openHoursLabel.text = @"Closed";
    }
}

- (void)loadLocation {
    SBWeakify(self);
    [SBSDK.shared getLocationDetailWithId:self.location.identifier completion:^(id location, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
        } else {
            self.location = location;
            [self reloadUI];
        }
    }];
}

- (void)openReservationList {
    [self.navigationController popViewControllerAnimated:YES];
    [self.tabBarController setSelectedIndex:SBUIMainTabbarItemReservations];
}

@end
