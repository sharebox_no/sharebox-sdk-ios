//
//  SBReservationsViewController.m
//  ShareboxSDK


#import "SBReservationsViewController.h"
#import "SBReservationCell.h"
@import ShareboxSDK;
#import "UIViewController+Alert.h"
#import "SBUI.h"
#import "SBReservationDetailViewController.h"

@interface SBReservationsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray<SBReservation *> *reservations;
@property (nonatomic, strong) NSArray<SBReservation *> *finishedReservations;

@property (nonatomic) id<SBCancelable> reservationRequest;
@property (nonatomic) id<SBCancelable> finishedReservationRequest;
@end

@implementation SBReservationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:@"SBReservationCell" bundle:[NSBundle bundleForClass:[SBReservationCell class]]];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SBReservationCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //reload list every time appear
    [self loadReservations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//MARK: - navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SBUI.reservationDetailSegueId]) {
        SBReservationDetailViewController *vc = segue.destinationViewController;
        NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
        SBReservation *reservation;
        if (indexPath.section == 0) {
            reservation = self.reservations[indexPath.row];
        } else {
            reservation = self.finishedReservations[indexPath.row];
        }
        vc.reservation = reservation;
    }
}


//MARK: - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.reservations.count;
    } else {
        return self.finishedReservations.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SBReservationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SBReservationCell"];
    
    SBReservation *reservation;
    if (indexPath.section == 0) {
        reservation = self.reservations[indexPath.row];
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        reservation = self.finishedReservations[indexPath.row];
        cell.backgroundColor = [UIColor grayColor];
    }
    [cell fillWithReservation:reservation];
    return cell;
}
//MARK: - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //show detail
    [self performSegueWithIdentifier:SBUI.reservationDetailSegueId sender:nil];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Active reservation";
    } else {
        return @"History";
    }
}

//MARK: - internals
- (void)loadReservations {
    //cancel previous load
    [self.reservationRequest cancel];
    [self.finishedReservationRequest cancel];
    
    SBWeakify(self);
    self.reservationRequest = [SBSDK.shared getReservations:^(NSArray *reservations, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            self.reservations = reservations;
            [self.tableView reloadData];
        }
    }];
    
    self.finishedReservationRequest = [SBSDK.shared getReservationsHistory:^(NSArray *reservations, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            self.finishedReservations = reservations;
            [self.tableView reloadData];
        }
    }];
}
@end
