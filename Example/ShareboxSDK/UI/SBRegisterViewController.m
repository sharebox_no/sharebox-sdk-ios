//
//  SBRegisterViewController.m
//  ShareboxSDK


#import "SBRegisterViewController.h"
@import ShareboxSDK;
#import "SBUI.h"

@interface SBRegisterViewController ()
@property (nonatomic, weak) IBOutlet UITextField *firstNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@end

@implementation SBRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.phoneTextField.enabled = false;
    self.phoneTextField.text = SBSDK.shared.phoneToVerify;
}

//MARK: - actions
- (IBAction)registerUser:(id)sender {
    NSString *firstName = self.firstNameTextField.text;
    NSString *lastName = self.lastNameTextField.text;
    NSString *email = self.emailTextField.text;
    
    BOOL isValid = firstName.length > 0 && lastName.length > 0 && email.length > 0;
    if (!isValid) {
        return;
    }
    
    SBWeakify(self);
    [SBSDK.shared registerWithFirstName:firstName lastname:lastName email:email completion:^(NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
        } else {
            //get profile
            [self getProfile];
        }
    }];
}

//MARK: - internals
- (void)getProfile {
    SBWeakify(self);
    [SBSDK.shared getProfile:^(id userProfile, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
        } else {
            //show main page
            [self showMainPage];
        }
    }];
}

- (void)showMainPage {
    UITabBarController *vc = [SBUI mainTabbarController];
    [self.navigationController setViewControllers:@[vc] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
