//
//  SBReservationDetailViewController.h
//  ShareboxSDK


#import <UIKit/UIKit.h>
@class SBReservation;

@interface SBReservationDetailViewController : UIViewController
@property(nonatomic) SBReservation *reservation;
@end
