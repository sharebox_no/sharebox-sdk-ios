//
//  UIViewController+Alert.h
//  ShareboxSDK


#import <UIKit/UIKit.h>

@interface UIViewController (Alert)

- (nonnull UIAlertController *)presentAlertWithTitle:(nullable NSString *)title
                                             message:(nullable NSString *)message
                                         cancelTitle:(nonnull NSString *)cancelTitle;
- (nonnull UIAlertController *)presentAlertWithTitle:(nullable NSString *)title
                                             message:(nullable NSString *)message
                                         cancelTitle:(nonnull NSString *)cancelTitle
                                         actionTitle:(nullable NSString *)actionTitle
                                              action:(void (^ _Nullable)(void))actionBlock;
- (nonnull UIAlertController *)presentAlertWithTitle:(nullable NSString *)title
                                             message:(nullable NSString *)message
                                             actions:(nullable NSArray<UIAlertAction *> *)actions;

- (nonnull UIAlertController *)presentError:(nonnull NSError *)error
                                cancelTitle:(nonnull NSString *)cancelTitle;
@end
