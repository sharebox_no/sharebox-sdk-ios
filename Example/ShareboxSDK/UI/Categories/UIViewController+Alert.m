//
//  UIViewController+Alert.m
//  ShareboxSDK


#import "UIViewController+Alert.h"

@implementation UIViewController (Alert)

- (UIViewController *)topPresentedViewController {
    UIViewController *target = self;
    while (target.presentedViewController) {
        target = target.presentedViewController;
    }
    return target;
};

- (UIViewController *)topVisibleViewController {
    if ([self isKindOfClass:[UINavigationController class]]) {
        return [(UINavigationController *)self topViewController].topVisibleViewController;
    } else if ([self isKindOfClass:[UITabBarController class]]) {
        return [(UITabBarController *)self selectedViewController].topVisibleViewController;
    } else {
        return self;
    }
};

- (UIViewController *)topMostViewController {
    return self.topPresentedViewController.topVisibleViewController;
};

- (UIAlertController *)presentAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle {
    return [self presentAlertWithTitle:title message:message cancelTitle:cancelTitle actionTitle:nil action:nil];
}

- (UIAlertController *)presentAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle actionTitle:(NSString *)actionTitle action:(void (^)(void))actionBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    if (actionTitle.length > 0) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            actionBlock();
        }];
        [alert addAction:action];
    }
    [[self topMostViewController] presentViewController:alert animated:YES completion:nil];
    return alert;
}

- (nonnull UIAlertController *)presentAlertWithTitle:(nullable NSString *)title
                                             message:(nullable NSString *)message
                                             actions:(nullable NSArray<UIAlertAction *> *)actions {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [actions enumerateObjectsUsingBlock:^(UIAlertAction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [alert addAction:obj];
    }];
    [[self topMostViewController] presentViewController:alert animated:YES completion:nil];
    return alert;
}

- (nonnull UIAlertController *)presentError:(nonnull NSError *)error
                                cancelTitle:(nonnull NSString *)cancelTitle {
    return [self presentAlertWithTitle:@(error.code).description message:error.domain cancelTitle:cancelTitle];
}
@end
