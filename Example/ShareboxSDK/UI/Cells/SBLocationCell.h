//
//  SBLocationCell.h
//  ShareboxSDK


#import <UIKit/UIKit.h>
@class SBStoreLocation;
@import CoreLocation;

@interface SBLocationCell : UITableViewCell

- (void)fillWithLocation:(SBStoreLocation *)location userLocation:(CLLocation *)userLocation;

@end
