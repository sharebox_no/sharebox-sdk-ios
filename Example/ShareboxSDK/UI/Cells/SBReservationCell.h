//
//  SBReservationCell.h
//  ShareboxSDK


#import <UIKit/UIKit.h>
@class SBReservation;

@interface SBReservationCell : UITableViewCell
- (void)fillWithReservation:(SBReservation *)reservation;
@end
