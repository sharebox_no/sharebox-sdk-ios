//
//  SBLocationCell.m
//  ShareboxSDK


#import "SBLocationCell.h"

@import ShareboxSDK;
@interface SBLocationCell()
@end

@implementation SBLocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillWithLocation:(SBStoreLocation *)location
            userLocation:(CLLocation *)userLocation {
    self.textLabel.text = location.name;
    self.detailTextLabel.text = location.fullAddress;
    self.detailTextLabel.numberOfLines = 0;
    if (userLocation) {
        CLLocationDistance distance = [SBSDK distanceFromUserLocation:userLocation.coordinate toStore:location];
        NSString *s = [NSString stringWithFormat:@"\n%.2f meters", distance];
        self.detailTextLabel.text = [self.detailTextLabel.text stringByAppendingString:s];
    } else {
        
    }
}

@end
