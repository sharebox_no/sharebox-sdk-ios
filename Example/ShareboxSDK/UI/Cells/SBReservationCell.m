//
//  SBReservationCell.m
//  ShareboxSDK


#import "SBReservationCell.h"
@import ShareboxSDK;

@interface SBReservationCell()
@end

@implementation SBReservationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textLabel.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillWithReservation:(SBReservation *)reservation {
    NSString *desc = @"";
    SBReservationUser *firstRecipient = reservation.recipients.firstObject;
    if (firstRecipient) {
        desc = [desc stringByAppendingFormat:@"%@", (firstRecipient.name?:firstRecipient.phone)];
    }
    desc = [desc stringByAppendingFormat:@"\n%@", reservation.location.fullAddress];
    
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"dd MM yyyy, HH:mm";
    
    desc = [desc stringByAppendingFormat:@"\n%@", [df stringFromDate:reservation.date]];
    if (reservation.type == SBReservationTypeFree) {
        desc = [desc stringByAppendingFormat:@"Number of free hours: \n%ld", (long)reservation.freeHours];
    }
    
    self.textLabel.text = desc;
}
@end
