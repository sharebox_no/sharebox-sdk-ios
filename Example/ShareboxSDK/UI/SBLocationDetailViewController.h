//
//  SBLocationDetailViewController.h
//  ShareboxSDK


#import <UIKit/UIKit.h>

@class SBStoreLocation;
@interface SBLocationDetailViewController : UIViewController
@property(nonatomic) SBStoreLocation *location;
@end
