//
//  SBReservationDetailViewController.m
//  ShareboxSDK


#import "SBReservationDetailViewController.h"
@import ShareboxSDK;
#import "UIViewController+Alert.h"

//@import CoreLocation;
@import MapKit;

//MARK: - Serializer
typedef NS_ENUM(NSUInteger, SBStoreDistanceType) {
    SBStoreDistanceUnknown,
    SBStoreDistanceClose,
    SBStoreDistanceRemote
};

@interface SBReservationDetailViewController ()
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UILabel *locationNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UILabel *openHoursLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *recipientName;
@property (nonatomic, weak) IBOutlet UILabel *recipientPhone;
@property (nonatomic, weak) IBOutlet UILabel *lockerNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *lockerStateLabel;

@property (nonatomic, weak) IBOutlet UIButton *openButton;
@property (nonatomic, weak) IBOutlet UIButton *finishButton;
@property (nonatomic, weak) IBOutlet UILabel *eventsLabel;

@property (nonatomic) NSArray<SBReservationEvent *> *events;
@property (nonatomic) CLLocationManager *locationManager;

@end

@implementation SBReservationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.showsUserLocation = YES;
    CLLocationManager *locationManager = [CLLocationManager new];
    [locationManager requestWhenInUseAuthorization];
    self.locationManager = locationManager;
    
    [self reloadUI];
    [self loadReservation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingLocation];
}

//MARK: - actions
- (IBAction)openLocker:(id)sender {
    if (self.mapView.userLocation.location) {
        [self requestOpenLocker:NO finish:NO fromLocation:self.mapView.userLocation.location];
    } else {
        [self warningNotDetermindLocationWithFinish:NO];
    }
}

- (IBAction)finishLocker:(id)sender {
    UIAlertAction *finishOnly = [UIAlertAction actionWithTitle: @"Exit reservation only" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self finishLockerOnly];
    }];
    UIAlertAction *openAndFinish = [UIAlertAction actionWithTitle: @"Exit reservation and open box" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self openLockerAndFinish];
    }];

    [self presentAlertWithTitle: @"What do you want to do?" message:nil actions:@[finishOnly, openAndFinish]];
}

- (void)warningNotDetermindLocationWithFinish:(BOOL)isFinish {
    NSString *warning = @"We cannot determine your location and check if you are near the correct locker. Do you want to proceed anyway?";
    [self presentAlertWithTitle:warning
                        message:nil
                    cancelTitle:@"Cancel"
                    actionTitle:isFinish ? @"Finish" : @"Open box"
                         action:^{
                             [self requestOpenLocker:YES finish:isFinish fromLocation:nil];
                         }];
}

- (void)warningFarTheBoxWithFinish:(BOOL)isFinish {
    NSString *warning = isFinish ? @"You are too far from the locker, do you want to finish the reservation remotely?" : @"You are too far from the locker, do you want to open it remotely?";
    
    [self presentAlertWithTitle:warning
                        message:nil
                    cancelTitle:@"cancel"
                    actionTitle:isFinish ? @"finish" :@"open_box"
                         action:^{
                             [self requestOpenLocker:YES finish:isFinish fromLocation:nil];
                         }];
}

//MARK: - internals
- (void)reloadUI {
    SBStoreLocation *location = self.reservation.location;
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(location.latitude.doubleValue, location.longitude.doubleValue);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.005, 0.005));
    //[self.mapView setCenterCoordinate:coordinate animated:YES];
    [self.mapView setRegion:region animated:YES];
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    //[annotation setTitle:@"Title"];
    //[annotation setSubtitle:@"sub title"];
    [self.mapView addAnnotation:annotation];
    
    self.locationNameLabel.text = location.name;
    self.addressLabel.text = location.address;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    SBOpenHours *openHours = [location findOpenHoursForWeekday:dateComponents.weekday];
    if (openHours) {
        self.openHoursLabel.text = openHours.is24Hours ? @"Open 24 hours" : openHours.timeRangeString;
    } else {
        self.openHoursLabel.text = @"Closed";
    }
    
    self.nameLabel.text = self.reservation.name;
    self.recipientName.text = self.reservation.owner.name;
    self.recipientPhone.text = self.reservation.owner.phone;
    self.lockerNumberLabel.text = @(self.reservation.lockerNumber).description;
    self.lockerStateLabel.text = self.reservation.isOpen ? @"Opened" : @"Closed";
    
    self.openButton.enabled = self.reservation.canOpen;
    self.finishButton.hidden = !self.reservation.canFinish;
    
    __block NSMutableArray *eventStrings = [NSMutableArray array];
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"dd MM yyyy, HH:mm";
    
    [self.events enumerateObjectsUsingBlock:^(SBReservationEvent *event, NSUInteger idx, BOOL *stop) {
        NSString *type = event.type == SBReservationEventTypeCreated ? @"Created" : @"Opened";
        NSString *line = [NSString stringWithFormat:@"%@ %@ av %@", [df stringFromDate:event.date], type, event.user.name];
        [eventStrings addObject:line];
    }];
    self.eventsLabel.text = [eventStrings componentsJoinedByString:@"\n"];
}

- (void)loadReservation {
    SBWeakify(self);
    [SBSDK.shared getReservationDetailWithId:self.reservation.identifier completion:^(SBReservationInfo *reservationInfo, NSError *error) {
        SBStrongify(self);
        if (error) {
            [self presentError:error cancelTitle:@"OK"];
        } else {
            self.reservation = reservationInfo.reservation;
            self.events = reservationInfo.events;
            [self reloadUI];
        }
    }];
}

- (void)requestOpenLocker:(BOOL)forceOpen finish:(BOOL)isFinish fromLocation:(CLLocation *)location {
    NSNumber *lat = forceOpen ? @(0) : @(location.coordinate.latitude);
    NSNumber *log = forceOpen ? @(0) : @(location.coordinate.longitude);
    
    SBWeakify(self);
    [SBSDK.shared openLockerWithId:self.reservation.lockerId latitude:lat longitude:log forceOpen:forceOpen finishReservation:isFinish completion:^(NSError * error) {
        SBStrongify(self);
        if (error) {
            //if user is far -> warnings
            if (error.code == 105) {
                [self warningFarTheBoxWithFinish:isFinish];
            } else {
                [self presentError:error cancelTitle:@"OK"];
            }
        } else {
            //finish -> back to the list
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)finishLockerOnly {
    SBWeakify(self);
    self.finishButton.enabled = NO;
    [SBSDK.shared stopSubscription:self.reservation.subscriptionId completion:^(NSError *error) {
        SBStrongify(self);
        if (error) {
            [self presentError:error cancelTitle:@"OK"];
        } else {
            [self loadReservation];
        }
        self.finishButton.enabled = YES;
    }];
}

- (void)openLockerAndFinish {
    if (self.mapView.userLocation.location) {
        [self requestOpenLocker:NO finish:YES fromLocation:self.mapView.userLocation.location];
    } else {
        [self warningNotDetermindLocationWithFinish:YES];
    }
}

@end
