//
//  SBProfileViewController.m
//  ShareboxSDK


#import "SBProfileViewController.h"
@import ShareboxSDK;

@interface SBProfileViewController ()
@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@end

@implementation SBProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SBUserProfile *profile = [SBSDK.shared user];
    self.firstNameLabel.text = profile.firstName;
    self.lastNameLabel.text = profile.lastName;
    self.emailLabel.text = profile.email;
    self.phoneLabel.text = profile.phone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logout:(id)sender {
    [SBSDK.shared logout];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
