//
//  SBUI.h
//  ShareboxSDK


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class SBLoginViewController;
@class SBRegisterViewController;
@class SBLocationsViewController;
@class SBLocationDetailViewController;
@class SBReservationsViewController;
@class SBReservationDetailViewController;
@class SBProfileViewController;
@class SBConsentViewController;
//MARK: - ParameterEncoding
typedef NS_ENUM(NSUInteger, SBUIMainTabbarItem) {
    SBUIMainTabbarItemLocations = 0,
    SBUIMainTabbarItemReservations,
    SBUIMainTabbarItemProfile
};

@interface SBUI : NSObject
/*
 view controllers
 */
+ (nonnull UIStoryboard *)storyboard;
+ (nonnull UITabBarController *)mainTabbarController;
+ (nonnull SBConsentViewController *)consentViewController;
+ (nonnull SBLoginViewController *)loginViewController;
+ (nonnull SBRegisterViewController *)registerViewController;
+ (nonnull SBLocationsViewController *)locationsViewController;
+ (nonnull SBLocationDetailViewController *)locationDetailViewController;
+ (nonnull SBReservationsViewController *)reservationsViewController;
+ (nonnull SBReservationDetailViewController *)reservationDetailViewController;
+ (nonnull SBProfileViewController *)profileViewController;

+ (nonnull NSString *)locationDetailSegueId;
+ (nonnull NSString *)reservationDetailSegueId;
@end

@interface SBUI (EntryPoint)
+ (nonnull UIViewController *)entryViewController;
@end
