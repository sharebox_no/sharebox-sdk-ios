//
//  SBConsentViewController.m
//  ShareboxSDK_Example


#import "SBConsentViewController.h"
#import "UIViewController+Alert.h"

@import WebKit;
@import ShareboxSDK;

@interface SBConsentViewController ()
@property (nonatomic) WKWebView *webview;

@end

@implementation SBConsentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    WKWebView *wv = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:[WKWebViewConfiguration new]];
    self.webview = wv;
    [self.view addSubview:wv];
    [self.view sendSubviewToBack:wv];
    
    SBWeakify(self);
    [SBSDK.shared getConsentWithCompletion:^(SBConsent *consent, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            [[SBKeychain shared] setString:@(consent.versionCode).description forKey:@"Consent Version"];
            [self.webview loadHTMLString:consent.content baseURL:nil];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)acceptConsent:(id)sender {
    SBWeakify(self);
    NSInteger consentVersion = [[[SBKeychain shared] stringForKey:@"Consent Version"] integerValue];
    if (!SBSDK.shared.isLoggedIn) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [SBSDK.shared grantConsentVersion:consentVersion completion:^(NSError *error) {
        SBStrongify(self);
        if (error) {
            [self presentError:error cancelTitle:@"OK"];
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

@end
