//
//  SBLoginViewController.m
//  ShareboxSDK


#import "SBLoginViewController.h"
@import ShareboxSDK;
#import "SBUI.h"
#import "UIViewController+Alert.h"
#import "SBConsentViewController.h"

@interface SBLoginViewController ()
@property (nonatomic, weak) IBOutlet UIView *phoneContainerView;
@property (nonatomic, weak) IBOutlet UIView *smsContainerView;

@property (nonatomic, weak) IBOutlet UITextField *smsCodeTextField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@end

@implementation SBLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadUI];
    
    //load consent for first time appear
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showConsent];
    });
}

- (void)showConsent {
    SBConsentViewController *vc = [SBUI consentViewController];
    [self presentViewController:vc animated:YES completion:nil];
}

//MARK: - actions
- (IBAction)verifyPhone:(id)sender {
    NSString *phone = self.phoneTextField.text;
    if (![SBSDK isValidPhoneFormat:phone]) {
        return;
    }
    
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    SBWeakify(self);
    
    NSInteger consentVersion = [[[SBKeychain shared] stringForKey: @"Consent Version"] integerValue];
    
    [SBSDK.shared authenticatePhone:phone
                     consentVersion:consentVersion
                           deviceId:deviceId completion:^(NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            //show input sms
            [self reloadUI];
        }
    }];
}

- (IBAction)verifySMSCode:(id)sender {
    NSString *smsCode = self.smsCodeTextField.text;
    if (smsCode.length == 0) {
        return;
    }
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    SBWeakify(self);
    [SBSDK.shared loginWithSMSCode:smsCode deviceId:deviceId completion:^(NSError *error) {
        SBStrongify(self);
        if (error) {
            if (error.code == 3) {
                //if error is not register -> show register page
                [self showRegisterPage];
            } else {
                //show error
                [self presentError:error cancelTitle:@"OK"];
            }
        } else {
            //show main
            [self showMainPage];
        }
    }];
}

- (IBAction)resetLocalStorage:(id)sender {
    [SBSDK.shared logout];
    [self reloadUI];
}

//MARK: - internals
- (void)reloadUI {
    BOOL isVerificationRequested = SBSDK.shared.isVerificationRequested;
    self.smsContainerView.hidden = !isVerificationRequested;
    self.phoneContainerView.hidden = isVerificationRequested;
    self.phoneTextField.text = SBSDK.shared.phoneToVerify;
}

- (void)showRegisterPage {
    SBRegisterViewController *vc = [SBUI registerViewController];
    [self.navigationController setViewControllers:@[vc] animated:YES];
}

- (void)showMainPage {
    UITabBarController *vc = [SBUI mainTabbarController];
    [self.navigationController setViewControllers:@[vc] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
