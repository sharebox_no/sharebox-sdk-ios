//
//  SBLocationsViewController.m
//  ShareboxSDK


#import "SBLocationsViewController.h"
@import ShareboxSDK;
#import "SBUI.h"
#import "SBLocationCell.h"
#import "SBLocationDetailViewController.h"
#import "SBConsentViewController.h"
#import "UIViewController+Alert.h"

@import CoreLocation;

@interface SBLocationsViewController () <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray<SBStoreLocation *> *locations;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *userLocation;
@end

@implementation SBLocationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CLLocationManager *locationManager = [CLLocationManager new];
    [locationManager requestWhenInUseAuthorization];
    locationManager.delegate = self;
    self.locationManager = locationManager;
    
    UINib *nib = [UINib nibWithNibName:@"SBLocationCell" bundle:[NSBundle bundleForClass:[SBLocationCell class]]];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"LocationCell"];
    [self loadLocations];
    
    [self loadConsent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingLocation];
}

//MARK: - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SBUI.locationDetailSegueId]) {
        SBLocationDetailViewController *vc = segue.destinationViewController;
        NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
        SBStoreLocation *location = self.locations[indexPath.row];
        vc.location = location;
    }
}


//MARK: - tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.locations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SBLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
    SBStoreLocation *location = self.locations[indexPath.row];
    [cell fillWithLocation:location userLocation:self.userLocation];
    return cell;
}
//MARK: - tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:SBUI.locationDetailSegueId sender:nil];
}

//MARK: - internals
- (void)loadLocations {
    SBWeakify(self);
    [SBSDK.shared getLocations:^(NSArray *locations, NSError *error) {
        SBStrongify(self);
        if (error) {
            //show error
            [self presentError:error cancelTitle:@"OK"];
        } else {
            if (self.userLocation) {
                self.locations = [SBSDK sortStores:locations byDistanceToUserLocation:self.userLocation.coordinate];
            } else {
                self.locations = locations;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)loadConsent {
    SBWeakify(self);
    [SBSDK.shared getConsentWithCompletion:^(SBConsent *consent, NSError *error) {
        SBStrongify(self);
        if (!error) {
            NSInteger storedConsentCode = [[[SBKeychain shared] stringForKey:@"Consent Version"] integerValue];
            if (storedConsentCode != consent.versionCode) {
                SBConsentViewController *vc = SBUI.consentViewController;
                [self presentViewController:vc animated:YES completion:nil];
            } else {
                NSLog(@"Consent wasn't changed");
            }
        }
    }];
}

//MARK: - location services
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (!self.userLocation && self.locations.count > 0) {
        [self.tableView reloadData];
    } else {
        self.userLocation = locations.lastObject;
    }
}
@end
