//
//  SBUI.m
//  ShareboxSDK


#import "SBUI.h"

@implementation SBUI

+ (UIStoryboard *)storyboard {
    return [UIStoryboard storyboardWithName:@"SBUI" bundle:[NSBundle bundleForClass:[self class]]];
}

+ (id)viewControllerWithId:(NSString *)identifier {
    return [[self storyboard] instantiateViewControllerWithIdentifier:identifier];
}

+ (UITabBarController *)mainTabbarController {
    return [self viewControllerWithId:@"MainTabbar"];
}

+ (SBConsentViewController *)consentViewController {
    return [self viewControllerWithId:@"Consent"];
}

+ (SBLoginViewController *)loginViewController {
    return [self viewControllerWithId:@"Login"];
}

+ (SBRegisterViewController *)registerViewController {
    return [self viewControllerWithId:@"Register"];
}

+ (SBLocationsViewController *)locationsViewController {
    return [self viewControllerWithId:@"Locations"];
}

+ (SBLocationDetailViewController *)locationDetailViewController {
    return [self viewControllerWithId:@"LocationDetail"];
}

+ (SBReservationsViewController *)reservationsViewController {
    return [self viewControllerWithId:@"Reservations"];
}

+ (SBReservationDetailViewController *)reservationDetailViewController {
    return [self viewControllerWithId:@"ReservationDetail"];
}

+ (SBProfileViewController *)profileViewController {
    return [self viewControllerWithId:@"Profile"];
}

+ (NSString *)locationDetailSegueId {
    return @"LocationDetail";
}

+ (NSString *)reservationDetailSegueId {
    return @"ReservationDetail";
}
@end

@import ShareboxSDK;
@implementation SBUI (EntryPoint)
+ (UIViewController *)entryViewController {
    if (SBSDK.shared.isLoggedIn) {
        UITabBarController *mainPage = [self mainTabbarController];
        return mainPage;
    } else if (SBSDK.shared.isRegistrationNeeded) {
        SBRegisterViewController *vc = [self registerViewController];
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:(UIViewController *)vc];
        return nvc;
    } else {
        SBLoginViewController *vc = [self loginViewController];
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:(UIViewController *)vc];
        return nvc;
    }
}
@end
