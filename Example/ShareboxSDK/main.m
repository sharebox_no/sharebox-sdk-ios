//
//  main.m
//  ShareboxSDK


@import UIKit;
#import "SBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SBAppDelegate class]));
    }
}
